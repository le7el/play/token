# LE7EL token smart contracts

## token/EESeedToken.sol
ERC-20 token with custom ERC-1404 implementation to limit transfers to whitelisted contract address, which will be used for the future conversion to Greet ecosystem token (EE).

## Swap.sol
Mints ERC-20 seed tokens for payment token contribution (USDC) by a fixed exchange rate. All contributed tokens go straight to [LE7EL DAO multisig](https://gnosis-safe.io/app/#/safes/0x96Cfb7a2B265575EAf0aa1949263AeD63a4D31c0/balances).

Uses whitelisting businesses logic for participation, inspired by [Gnosis Auction code](https://github.com/gnosis/ido-contracts/blob/main/contracts/allowListExamples/AllowListOffChainManaged.sol) and the [following article](https://medium.com/@PhABC/off-chain-whitelist-with-on-chain-verification-for-ethereum-smart-contracts-1563ca4b8f11).

## token/L7LToken.sol
Modern ERC-20 token with permit feature to save gas costs on swaps, the token code is kept lean to reduce potential attack exposure. The maximum supply is limited to 1b tokens, token owner can mint new tokens until the maximum supply is reached. __MultiMinter.sol__ is intended to be the owner of this contract.

## MultiMinter.sol
Transient contract which supports management of token minters. LE7EL DAO is supposed to be an ultimate owner of this contract. Minting rights would be given to LE7EL DAO address, swap contract for investors to exchange their seed tokens __vesting/RedeemSeedByMint.sol__, reward pools for contributors, creators and operators. This contract is upgradable through `transferOwnership`.

## vesting/RedeemSeedByMint.sol
Redeem seed tokens to L7L according to the vesting schedule.

# Vesting distribution
[Virtual Distributor](https://gitlab.com/le7el/build/rewards_engine/-/blob/master/contracts/VirtualDistributor.sol) can be used to distribute vested amounts to investors. 

# Local installation
Expects Ganache or similar Ethereum client on `127.0.0.1:8545`, default `network_id` is 5777.

    npm install
    npm run migrate:dev

# Testing
Signatures for whitelisting can be generated with the following cli command: `truffle exec tasks/generate_signatures.js --network=develop --file_with_address="../your_address_inputs.txt"`

`your_address_inputs.txt` contains coma separated whitelisted wallet. Can be generated with [Greet P2P KYC tool](https://kyc.le7el.com).

Default KYC signer wallet used for tests is `0x4253e41227F7af597176963A4F1e8399539e60D5`, it's derived from `nerve panda scare join quantum reform ski umbrella clap worth barely ask` test mnemonic. _Don't use it for anything other than local tests, it's insecure._

`tasks/generate_signatures.js` you can replace test mnemonic with your local Ganache mnemonic, in such case first wallet `provider.getAddress(0)` will be a whitelisting KYC signer.

You may need to update `CLIENT_WHITELIST_SIGNATURE` and `CLIENT_NO_MONEY_WHITELIST_SIGNATURE` in `test/seed_token.js` and `KYC_SIGNER` in `migrations/2_deploy_seed_token.js` for a local signer wallet. Alternatively `KYC_SIGNER` can be updated on a smart contract level, like: `swap.updateContractData.sendTransaction("YOUR_ABI_ENCODED_KYC_SIGNER_WALLET", ether("50000"), ether("150000"), { from: ADMIN })`.

To run automatic tests: 

    npm run test

# Deployments

To deploy, run scripts with the relevant environment variables: `source .secret && ADMIN=0xeeF30c8B265Ffa552781C1f8a5F2CECff3D3a84B BENEFICIARY=0xeeF30c8B265Ffa552781C1f8a5F2CECff3D3a84B VENDOR_TOKEN=0xa572D9e7153ECFe65795F8FCCeF489588B2f7a36 PAYMENT_TOKEN=0xBb0070c45ebE87926e48745BD2a7D73E349Df909 forge script script/DeploySwap.s.sol:DeploySwapScript --rpc-url $GOERLY_RPC_URL  --private-key $PRIVATE_KEY --broadcast --verify --etherscan-api-key $ETHERSCAN_KEY -vvvv --slow`.

Deploy native swap: `source .secret && ADMIN=0xeeF30c8B265Ffa552781C1f8a5F2CECff3D3a84B BENEFICIARY=0xeeF30c8B265Ffa552781C1f8a5F2CECff3D3a84B VENDOR_TOKEN=0xa572D9e7153ECFe65795F8FCCeF489588B2f7a36 forge script script/DeployNativeSwap.s.sol:DeployNativeSwapScript --rpc-url $GOERLY_RPC_URL  --private-key $PRIVATE_KEY --broadcast --verify --etherscan-api-key $ETHERSCAN_KEY -vvvv --slow`.

Deploy alternative token swap: `source .secret && ADMIN=0xeeF30c8B265Ffa552781C1f8a5F2CECff3D3a84B BENEFICIARY=0xeeF30c8B265Ffa552781C1f8a5F2CECff3D3a84B VENDOR_TOKEN=0xa572D9e7153ECFe65795F8FCCeF489588B2f7a36 PAYMENT_TOKEN=0x52a9CF7617102e7445f3Fe09BD935EDfFf3A8f4f forge script script/DeploySwap.s.sol:DeploySwapScript --rpc-url $GOERLY_RPC_URL  --private-key $PRIVATE_KEY --broadcast --verify --etherscan-api-key $ETHERSCAN_KEY -vvvv --slow`.

Deploy payment test token: `source .secret && forge script script/DeployTestTokens.s.sol:DeployTestTokensScript --rpc-url $SEPOLIA_RPC_URL  --private-key $PRIVATE_KEY --broadcast --verify --etherscan-api-key $ETHERSCAN_KEY -vvvv --slow`.

Deploy L7L token: `source .secret && ADMIN=0x1710476Dc31c9Ee497556d31634Fc4b1Ded07f9C forge script script/DeployL7L.s.sol:DeployL7LScript --rpc-url $SEPOLIA_RPC_URL --private-key $PRIVATE_KEY --broadcast --verify --etherscan-api-key $ETHERSCAN_KEY -vvvv --slow`.

Deploy seed redemption contract: `source .secret && ADMIN=0x1710476Dc31c9Ee497556d31634Fc4b1Ded07f9C L7L_MINTER_ADDRESS=0x5148fED4947c375a94f56f63CaCec56c6D88f857 SEED_TOKEN_ADDRESS=0xc566b9Ce143B73704Ca75AE3e9bDBDC5d069B3cF VESTING_START=1716414864 VESTING_END=1742690700 forge script script/DeploySeedRedemption.s.sol:DeploySeedRedemptionScript --rpc-url $SEPOLIA_RPC_URL --private-key $PRIVATE_KEY --broadcast --verify --etherscan-api-key $ETHERSCAN_KEY -vvvv --slow`.

Deploy shop: `source .secret && ADMIN=0x1710476Dc31c9Ee497556d31634Fc4b1Ded07f9C BENEFICIARY=0x1710476Dc31c9Ee497556d31634Fc4b1Ded07f9C forge script script/DeployShop.s.sol:DeployShopScript --rpc-url $SEPOLIA_RPC_URL --private-key $PRIVATE_KEY --broadcast --verify --etherscan-api-key $ETHERSCAN_KEY -vvvv --slow`

Update shop listing: `source .secret && forge script script/DeployUpdateShopListing.s.sol:DeployUpdateShopListingScript --rpc-url $SEPOLIA_RPC_URL --private-key $PRIVATE_KEY --broadcast -vvvv --slow`

Deploy IDO shop: `source .secret && ADMIN=0x1710476Dc31c9Ee497556d31634Fc4b1Ded07f9C BENEFICIARY=0x1710476Dc31c9Ee497556d31634Fc4b1Ded07f9C DISCOUNT_NFT=0xd8f803bFB0Fb6f4717A2791999884d39f92d2095 SEED_TOKEN=0xc566b9Ce143B73704Ca75AE3e9bDBDC5d069B3cF WETH_ADDRESS=0xF59F671789FB2204b87f6c4405CDf2A6cdA585AA USDC_ADDRESS=0x8EEdc0459D28A10Dd58d83CB528815817056302d USDT_ADDRESS=0xaFf5366Ea9a5a29A171658C05B1912eE4b1FB293 forge script script/DeployIDOShop.s.sol:DeployIDOShopScript --rpc-url $SEPOLIA_RPC_URL --private-key $PRIVATE_KEY --broadcast --verify --etherscan-api-key $ETHERSCAN_KEY -vvvv --slow`


## Mainnet

* L7L token: 0xeA46Bf7Fe6c9a2e29a97D2Da2775131067b4BA0C
* MultiMinter: 0x83De2fb56eAaE65ac07378d0e48b7c59E76b50a4
* EESeedToken: 0x77c2F5cB9f191af74FeB1669A8cC4774c654C68E
* Pre-seed Swap: 0xEb6Bc0852e65F7dA61420777eA5E32C650e90CfD
* Community USDT Swap: 0x0ea1b7B4B3b29f907E7EB78852dcEAB3F4Daa83d
* Community USDC Swap: 0xC115114b90D9cE90197860d4Bfa21F4a47392990
* Community ETH Swap: 0xbc19970A64311608bcF5aee57501717F108F84cf

## Polygon

* L7L token (bridged): 0x84a431Bd2C958414B2E316CEdd9F85993ace5000
* EESeedToken: 0xEE9ab2532378e9C24bb4f2661E818F2A36347E23
* L7 Investors Snapshot: 0xFE70889c5848E06BB68222d9376CED1f14fA09Bf
* Shop: 0x702b0d7B1ae097fB59f4CD8979aec7C06F68c626
* IDO Payment Manager: 0x2e19001639A09356b70C7c7ccF5c1f38950f1BE0
* EESeed Delivery Manager: 0x3C34AF6DDafB21C3A9293F1356E3684bF3d5a1bD

## opBNB

* Shop: 0x1B60e4A71f91590Db733aDe6A1b72F8D50d5fc73
* PerkManager: 0xB2a6B7dC13E8545cb035464F53Cf7D4ac8a408E5
* PaymentManager: 0x86eeF9a707128DB6682aB0CE05Bd62465D0D0e55
* GDXPaymentManager: 0x4B08725B4EdD7a41924888204798FFA14e85c41c
* DeliveryManager: 0x2e19001639A09356b70C7c7ccF5c1f38950f1BE0

## Sepolia

* L7L token: 0xF59F671789FB2204b87f6c4405CDf2A6cdA585AA
* EESeedToken: 0xc566b9Ce143B73704Ca75AE3e9bDBDC5d069B3cF
* Seed redemption: 0xB2a6B7dC13E8545cb035464F53Cf7D4ac8a408E5
* MultiMinter: 0x5148fED4947c375a94f56f63CaCec56c6D88f857
* Rewarder: 0x86eeF9a707128DB6682aB0CE05Bd62465D0D0e55
* Shop v1: 0x6908D59A7Ae071bCBd97BB4a8F6e5913eb3eB6f6
* Shop v2: 0x130542ce1Cf6FcCaf880bD8021cE38b9dD520BDa
* PerkManager: 0x360C16a55FA92c9BB2FC13f84943eB6ece7f9A3E
* PaymentManager: 0x4A18D2C90d345998a720F538ed40debbFf90aaeE
* IDO PaymentManager: 0xFc152801c0B3981f843C92f230de7675C72aa66c
* PerkDeliveryManager: 0x5Ce2430Feccc0287836d7E3895cEB59C56c65B76
* EESeed DeliveryManager: 0xf6897089e73e38675907d0d5D01DE231D51a2080
* MockUSDC: 0x8EEdc0459D28A10Dd58d83CB528815817056302d
* MockUSDT: 0xaFf5366Ea9a5a29A171658C05B1912eE4b1FB293
* L7 Investors Snapshot: 0xd2E424106C46117E6b51500346c7ec84e5B04f8b

## Goerli

* EESeedToken: 0xa572D9e7153ECFe65795F8FCCeF489588B2f7a36
* Native Swap: 0x17bf3EF0AebA3a157a2541b595E35628dC61Eedd
* Swap: 0x01FFEaEd1896291dd4Fd67293e846e613D11f6bA
* PaymentTokenMock: 0xBb0070c45ebE87926e48745BD2a7D73E349Df909
* Swap (alternative): 0xb78693478E03788f242f6DFE4C03199C0b16c4D3
* PaymentTokenMock (alternative): 0x52a9CF7617102e7445f3Fe09BD935EDfFf3A8f4f
* DaiTokenMock: 0xE83032B50e02938223be6f288F9fF88676C29A91

## Rinkeby

* EESeedToken: 0xBbbe3249AADbafC155Ada7D82731346e43531D30
* PaymentTokenMock: 0x08E89A5f292398cc1581655592E2E6C9c11Ab272
* DaiTokenMock: 0x259137a40745D35a0fbe6aD8c89eA1e611c303F4
* Swap: 0x3909bD2ac33bBd341f6eED7f272785B6B88319Fc

# Direct interactions with smart contract by DAO

1) Provide address which will call smart contract to Greet team and get confirmation that it's whitelisted and `SIGNATURE` value;
2) Send `approve(0xEb6Bc0852e65F7dA61420777eA5E32C650e90CfD, 250000000000)` transaction to `0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48`;
3) Send `contribute(250000000000, 0xd183d7aadc13ca77a0189b5355e86039be51773704ce672f08afbfb1527400e9, SIGNATURE)` transaction to `0xEb6Bc0852e65F7dA61420777eA5E32C650e90CfD`;
4) Check ERC20 balance of seedEE for `0x77c2F5cB9f191af74FeB1669A8cC4774c654C68E` token address.

# Verification on non-etherscan blockchain explorers

`forge verify-contract ADDRESS PATH:CONTRACTNAME --optimizer-runs=200 --constructor-args $(cast abi-encode "constructor(address)" "0x1710476Dc31c9Ee497556d31634Fc4b1Ded07f9") --show-standard-json-input > standard_json_output.json`