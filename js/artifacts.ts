import seedTokenArtifact from '../out/EESeedToken.sol/EESeedToken.json'
import swapArtifact from '../out/Swap.sol/Swap.json'
import paymentTokenArtifact from '../out/PaymentTokenMock.sol/PaymentTokenMock.json'
import nativeSwapArtifact from '../out/NativeSwap.sol/NativeSwap.json'
import l7lTokenArtifact from '../out/L7LToken.sol/L7LToken.json'
import shopArtifact from '../out/Shop.sol/Shop.json'
import perkManagerArtifact from '../out/PerkManager.sol/PerkManager.json'
import redeemSeedByMintArtifact from '../out/RedeemSeedByMint.sol/RedeemSeedByMint.json'

export default {
  seedTokenArtifact,
  swapArtifact,
  nativeSwapArtifact,
  paymentTokenArtifact,
  l7lTokenArtifact,
  shopArtifact,
  perkManagerArtifact,
  redeemSeedByMintArtifact
}