export default {
  '5': {
    'v1': {
      native_swap: "0x17bf3EF0AebA3a157a2541b595E35628dC61Eedd",
      swap: "0x01FFEaEd1896291dd4Fd67293e846e613D11f6bA",
      usdc_swap: "0xb78693478E03788f242f6DFE4C03199C0b16c4D3",
      seed_token: "0xa572D9e7153ECFe65795F8FCCeF489588B2f7a36",
      payment_mock_token: "0xBb0070c45ebE87926e48745BD2a7D73E349Df909",
      alternative_payment_mock_token: "0x52a9CF7617102e7445f3Fe09BD935EDfFf3A8f4f"
    }
  },
  '11155111': {
    'v1': {
      l7l_token: "0xF59F671789FB2204b87f6c4405CDf2A6cdA585AA",
      multi_minter: "0x5148fED4947c375a94f56f63CaCec56c6D88f857",
      seed_redemption: "0xB2a6B7dC13E8545cb035464F53Cf7D4ac8a408E5",
      seed_token: "0xc566b9Ce143B73704Ca75AE3e9bDBDC5d069B3cF",
      team_rewarder: "0x86eeF9a707128DB6682aB0CE05Bd62465D0D0e55",
      shop: "0x6908D59A7Ae071bCBd97BB4a8F6e5913eb3eB6f6",
      perk_manager: "0x360C16a55FA92c9BB2FC13f84943eB6ece7f9A3E",
      payment_manager: "0x4A18D2C90d345998a720F538ed40debbFf90aaeE",
      ido_payment_manager: "0xFc152801c0B3981f843C92f230de7675C72aa66c",
      perk_delivery_manager: "0x5Ce2430Feccc0287836d7E3895cEB59C56c65B76",
      seed_delivery_manager: "0xf6897089e73e38675907d0d5D01DE231D51a2080",
      payment_mock_token: "0x8EEdc0459D28A10Dd58d83CB528815817056302d",
      alternative_payment_mock_token: "0xaFf5366Ea9a5a29A171658C05B1912eE4b1FB293",
      l7_investors_snapshot: "0xd2E424106C46117E6b51500346c7ec84e5B04f8b"
    },
    'v2': {
      shop: "0x130542ce1Cf6FcCaf880bD8021cE38b9dD520BDa"
    }
  },
  '137': {
    'v1': {
      l7l_token: "0x84a431Bd2C958414B2E316CEdd9F85993ace5000",
      seed_token: "0xEE9ab2532378e9C24bb4f2661E818F2A36347E23",
      l7_investors_snapshot: "0xFE70889c5848E06BB68222d9376CED1f14fA09Bf",
      ido_payment_manager: "0x2e19001639A09356b70C7c7ccF5c1f38950f1BE0",
      seed_delivery_manager: "0x3C34AF6DDafB21C3A9293F1356E3684bF3d5a1bD"
    },
    'v2': {
      shop: "0x702b0d7B1ae097fB59f4CD8979aec7C06F68c626"
    }
  }, 
  '1': {
    'v1': {
      l7l_token: "0xeA46Bf7Fe6c9a2e29a97D2Da2775131067b4BA0C",
      multi_minter: "0x83De2fb56eAaE65ac07378d0e48b7c59E76b50a4",
      native_swap: "0xbc19970A64311608bcF5aee57501717F108F84cf",
      swap: "0x0ea1b7B4B3b29f907E7EB78852dcEAB3F4Daa83d",
      usdc_swap: "0xC115114b90D9cE90197860d4Bfa21F4a47392990",
      seed_token: "0x77c2F5cB9f191af74FeB1669A8cC4774c654C68E"
    }
  },
  '204': {
    'v1': {
      shop: '0x1B60e4A71f91590Db733aDe6A1b72F8D50d5fc73',
      perk_manager: '0xB2a6B7dC13E8545cb035464F53Cf7D4ac8a408E5',
      payment_manager: '0x86eeF9a707128DB6682aB0CE05Bd62465D0D0e55',
      perk_delivery_manager: '0x2e19001639A09356b70C7c7ccF5c1f38950f1BE0'
    }
  }
}