import { ethers } from 'ethers'
import artifacts from './artifacts'
import untypedContracts from './contracts'
import Shop from './shop'
import PerkManager from './perk_manager'
import RedeemSeedByMint from './redeem_seed_by_mint'
import { ContractAddresses } from './utils'

const CURRENT_VERSION = 'v1'

const contracts: ContractAddresses = untypedContracts;

const assertContractDeployed = (chainId : number, contractName : string, version = CURRENT_VERSION) => {
  if (contracts[`${chainId}`] && contracts[`${chainId}`][version] && contracts[`${chainId}`][version][contractName]) {
    return Promise.resolve(contracts[chainId][version][contractName])
  }
  return Promise.reject(`contract ${contractName}:${version} is not deployed to network ${chainId}`);
}

export {
  ethers,
  artifacts,
  contracts,
  assertContractDeployed,
  Shop,
  PerkManager,
  RedeemSeedByMint
}