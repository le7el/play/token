import untypedContracts from "./contracts"
import artifacts from "./artifacts"
import { initContractByAbi, ContractAddresses, ValidProvider } from "./utils"

const PERK_MANAGER_CONTRACT = 'perk_manager'
const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000'

const contracts: ContractAddresses = untypedContracts;

const abi = () => {
  return artifacts.perkManagerArtifact.abi
}

const bytecode = () => {
  return artifacts.perkManagerArtifact.bytecode
}

const deployedAddress = (network : string | number) => {
  if (!contracts[`${network}`]) return null
  return contracts[`${network}`][PERK_MANAGER_CONTRACT]
}

const initContract = (contractKey : string, readOnly = true, web3Provider : ValidProvider | null = null) => {
  return initContractByAbi(abi(), contractKey, readOnly, web3Provider)
}

const registerPerk = (
  owner : string,
  totalSupply : number,
  exclusive : boolean,
  metadata : string,
  web3Provider : ValidProvider | null = null,
  contractKey = PERK_MANAGER_CONTRACT
) => {
  return initContract(contractKey, false, web3Provider)
    .then(contract => contract.registerPerk(owner, totalSupply, exclusive, metadata))
}

const updatePerk = (
  id : number,
  newOwner : string = ZERO_ADDRESS,
  totalSupply : number,
  exclusiveOff : boolean = false,
  metadata : string = "0x00",
  web3Provider : ValidProvider | null = null,
  contractKey = PERK_MANAGER_CONTRACT
) => {
  return initContract(contractKey, false, web3Provider)
    .then(contract => contract.updatePerk(id, newOwner, totalSupply, exclusiveOff, metadata))
}

const togglePerkManager = (
  id : number,
  manager : string,
  status : boolean,
  web3Provider : ValidProvider | null = null,
  contractKey = PERK_MANAGER_CONTRACT
) => {
  return initContract(contractKey, false, web3Provider)
    .then(contract => contract.togglePerkManager(id, manager, status))
}

const grantPerk = (
  id : number,
  user : string,
  amount : number,
  web3Provider : ValidProvider | null = null,
  contractKey = PERK_MANAGER_CONTRACT
) => {
  return initContract(contractKey, false, web3Provider)
    .then(contract => contract.grantPerk(id, user, amount))
}

const revokePerk = (
  id : number,
  user : string,
  amount : number,
  web3Provider : ValidProvider | null = null,
  contractKey = PERK_MANAGER_CONTRACT
) => {
  return initContract(contractKey, false, web3Provider)
    .then(contract => contract.revokePerk(id, user, amount))
}

const usePerk = (
  id : number,
  user : string,
  usageUid : string,
  web3Provider : ValidProvider | null = null,
  contractKey = PERK_MANAGER_CONTRACT
) => {
  return initContract(contractKey, false, web3Provider)
    .then(contract => contract.usePerk(id, user, usageUid))
}

const perk = (
  id : number,
  web3Provider : ValidProvider | null = null,
  contractKey = PERK_MANAGER_CONTRACT
) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.perk(id))
}

const perkManagers = (
  id : number,
  address : string,
  web3Provider : ValidProvider | null = null,
  contractKey = PERK_MANAGER_CONTRACT
) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.perkManagers(id, address))
}

const balanceOf = (
  user : string,
  id : number,
  web3Provider : ValidProvider | null = null,
  contractKey = PERK_MANAGER_CONTRACT
) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.balanceOf(user, id))
}

const balanceOfBatch = (
  users : string[],
  ids : number[],
  web3Provider : ValidProvider | null = null,
  contractKey = PERK_MANAGER_CONTRACT
) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.balanceOfBatch(users, ids))
}

const uri = (
  id : number,
  web3Provider : ValidProvider | null = null,
  contractKey = PERK_MANAGER_CONTRACT
) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.uri(id))
}

const perkUsages = (
  id : number,
  user : string,
  usageUid : string,
  web3Provider : ValidProvider | null = null,
  contractKey = PERK_MANAGER_CONTRACT
) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.perkUsages(id, user, usageUid))
}

const perkTotalUsages = (
  id : number,
  user : string,
  web3Provider : ValidProvider | null = null,
  contractKey = PERK_MANAGER_CONTRACT
) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.perkTotalUsages(id, user))
}

export {
  abi,
  bytecode,
  deployedAddress,
  registerPerk,
  updatePerk,
  togglePerkManager,
  grantPerk,
  revokePerk,
  usePerk,
  perk,
  perkManagers,
  balanceOf,
  balanceOfBatch,
  uri,
  perkUsages,
  perkTotalUsages
}
export default {
  abi,
  bytecode,
  deployedAddress,
  registerPerk,
  updatePerk,
  togglePerkManager,
  grantPerk,
  revokePerk,
  usePerk,
  perk,
  perkManagers,
  balanceOf,
  balanceOfBatch,
  uri,
  perkUsages,
  perkTotalUsages
}
