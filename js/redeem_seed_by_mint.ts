import untypedContracts from "./contracts"
import artifacts from "./artifacts"
import { initContractByAbi, ContractAddresses, ValidProvider } from "./utils"

const SEED_REDEMPTION_CONTRACT = 'seed_redemption'
const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000'

const contracts: ContractAddresses = untypedContracts;

const abi = () => {
  return artifacts.redeemSeedByMintArtifact.abi
}

const bytecode = () => {
  return artifacts.redeemSeedByMintArtifact.bytecode
}

const deployedAddress = (network : string | number) => {
  if (!contracts[`${network}`]) return null
  return contracts[`${network}`][SEED_REDEMPTION_CONTRACT]
}

const initContract = (contractKey : string, readOnly = true, web3Provider : ValidProvider | null = null) => {
  return initContractByAbi(abi(), contractKey, readOnly, web3Provider)
}

const vestedAmountToClaim = (
  user : string,
  web3Provider : ValidProvider | null = null,
  contractKey = SEED_REDEMPTION_CONTRACT
) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.vestedAmountToClaim(user))
}

const vestingAmount = (
  user : string,
  web3Provider : ValidProvider | null = null,
  contractKey = SEED_REDEMPTION_CONTRACT
) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.vestingAmount(user))
}

const claimedAmount = (
  user : string,
  web3Provider : ValidProvider | null = null,
  contractKey = SEED_REDEMPTION_CONTRACT
) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.claimedAmount(user))
}

const claim = (
  owner : string,
  web3Provider : ValidProvider | null = null,
  contractKey = SEED_REDEMPTION_CONTRACT
) => {
  return initContract(contractKey, false, web3Provider)
    .then(contract => contract['claim(address)'](owner))
}

export {
  abi,
  bytecode,
  deployedAddress,
  vestedAmountToClaim,
  vestingAmount,
  claimedAmount,
  claim
}
export default {
  abi,
  bytecode,
  deployedAddress,
  vestedAmountToClaim,
  vestingAmount,
  claimedAmount,
  claim
}