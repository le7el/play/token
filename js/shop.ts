import untypedContracts from "./contracts"
import artifacts from "./artifacts"
import { initContractByAbi, ContractAddresses, ValidProvider } from "./utils"

const SHOP_CONTRACT = 'shop'

const contracts: ContractAddresses = untypedContracts;

const abi = () => {
  return artifacts.shopArtifact.abi
}

const bytecode = () => {
  return artifacts.shopArtifact.bytecode
}

const deployedAddress = (network : string | number) => {
  if (!contracts[`${network}`]) return null
  return contracts[`${network}`][SHOP_CONTRACT]
}

const initContract = (contractKey : string, readOnly = true, web3Provider : ValidProvider | null = null) => {
  return initContractByAbi(abi(), contractKey, readOnly, web3Provider)
}

const list = (
  uid : string,
  paymentMethod : object,
  ware : object,
  web3Provider : ValidProvider | null = null,
  contractKey = SHOP_CONTRACT
) => {
  return initContract(contractKey, false, web3Provider)
    .then(contract => contract.list(uid, paymentMethod, ware))
}

const delist = (
  uid : string,
  web3Provider : ValidProvider | null = null,
  contractKey = SHOP_CONTRACT
) => {
  return initContract(contractKey, false, web3Provider)
    .then(contract => contract.delist(uid))
}

const updateListing = (
  uid : string,
  seller : string,
  paymentMethod : object,
  ware : object,
  web3Provider : ValidProvider | null = null,
  contractKey = SHOP_CONTRACT
) => {
  return initContract(contractKey, false, web3Provider)
    .then(contract => contract.updateListing(uid, seller, paymentMethod, ware))
}

const exchange = (
  uid : string,
  bidTokenAmount : number,
  purchaseUnits : number,
  context : string,
  web3Provider : ValidProvider | null = null,
  contractKey = SHOP_CONTRACT
) => {
  return initContract(contractKey, false, web3Provider)
    .then(contract => contract.exchange(uid, bidTokenAmount, purchaseUnits, context))
}

const quoteFor = (
  uid : string,
  buyer : string,
  purchaseUnits : number,
  context : string,
  web3Provider : ValidProvider | null = null,
  contractKey = SHOP_CONTRACT
) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.quoteFor(uid, buyer, purchaseUnits, context))
}

const listings = (
  listingUid : string,
  web3Provider : ValidProvider | null = null,
  contractKey = SHOP_CONTRACT
) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.listings(listingUid))
}

const wareUid = (
  ware : object,
  web3Provider : ValidProvider | null = null,
  contractKey = SHOP_CONTRACT
) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.wareUid(ware))
}

const listingFee = (
  web3Provider : ValidProvider | null = null,
  contractKey = SHOP_CONTRACT
) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.listingFee())
}

const exchangeFee = (
  web3Provider : ValidProvider | null = null,
  contractKey = SHOP_CONTRACT
) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.exchangeFee())
}

const defaultPaymentManager = (
  web3Provider : ValidProvider | null = null,
  contractKey = SHOP_CONTRACT
) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.defaultPaymentManager())
}

const restrictedListing = (
  web3Provider : ValidProvider | null = null,
  contractKey = SHOP_CONTRACT
) => {
  return initContract(contractKey, true, web3Provider)
    .then(contract => contract.restrictedListing())
}

export {
  abi,
  bytecode,
  deployedAddress,
  initContract,
  list,
  delist,
  updateListing,
  exchange,
  quoteFor,
  listings,
  wareUid,
  listingFee,
  exchangeFee,
  defaultPaymentManager,
  restrictedListing
}
export default {
  abi,
  bytecode,
  deployedAddress,
  initContract,
  list,
  delist,
  updateListing,
  exchange,
  quoteFor,
  listings,
  wareUid,
  listingFee,
  exchangeFee,
  defaultPaymentManager,
  restrictedListing
}
