const Web3Utils = require('web3-utils');

const SwapContract = artifacts.require("Swap")
const SwapMockContract = artifacts.require("SwapMock")
const PaymentTokenMockContract = artifacts.require("PaymentTokenMock")
const EESeedTokenContract = artifacts.require("EESeedToken")

module.exports = async(deployer, network, accounts) => {
    let ADMIN = accounts[0];
    let BENEFICIARY = accounts.length == 1 ? accounts[0] : accounts[5];

    // 18 DECIMALS PAYMENT TOKEN
    //let EXCHANGE_RATE = 35; // 0.035 with 1000 as EXCHANGE_BASE for a normal 18 decimal token
    //let EXCHANGE_BASE = 1000;

    // USDC
    let EXCHANGE_RATE = '50' // +3 zeros to base for 0.05
    let EXCHANGE_BASE = '1000000000000000' // USDC / USDT have 6 decimals + 3 decimals for (0.05 rate), so 18-6+3=15 zeros
    let BONUS_EXCHANGE_RATE = '54'

    let PAYMENT_TOKEN; // mutable once
    const eeSeedTokenContract = await EESeedTokenContract.deployed()
    const VENDOR_TOKEN = eeSeedTokenContract.address

    const MINTER_ROLE = Web3Utils.soliditySha3("MINTER_ROLE")
    const DEFAULT_ADMIN_ROLE = "0x00"
    let KYC_SIGNER = "0x000000000000000000000000611d8ca43d3e53b6216a2dc25baf23d753867f16" // abi encoded 0x611d8ca43d3e53b6216a2dc25baf23d753867f16

    console.log("HERE")
    try {
        if (network !== 'live_mainnet') {
            const paymentTokenMockContract = await PaymentTokenMockContract.deployed()
            PAYMENT_TOKEN = paymentTokenMockContract.address
        } else {
            KYC_SIGNER = "0x0000000000000000000000000436dc08371fcef052e70e4101df41d5d02cfc42"
            PAYMENT_TOKEN = "0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48" // USDC e.g. https://www.coingecko.com/en/coins/usd-coin
            ADMIN = "0x96Cfb7a2B265575EAf0aa1949263AeD63a4D31c0" // Greet DAO multisig
            BENEFICIARY = "0x96Cfb7a2B265575EAf0aa1949263AeD63a4D31c0" // Greet DAO multisig
        }

        let swapContract
        if (network === 'develop') {
            swapContract = await deployer.deploy(
                SwapMockContract,
                ADMIN,
                BENEFICIARY,
                VENDOR_TOKEN,
                PAYMENT_TOKEN,
                EXCHANGE_RATE,
                EXCHANGE_BASE,
                BONUS_EXCHANGE_RATE,
                KYC_SIGNER
            )
        } else {
            console.log("BEFORE")
            swapContract = await deployer.deploy(
                SwapContract,
                ADMIN,
                BENEFICIARY,
                VENDOR_TOKEN,
                PAYMENT_TOKEN,
                EXCHANGE_RATE,
                EXCHANGE_BASE,
                BONUS_EXCHANGE_RATE,
                KYC_SIGNER
            )
        }

        await eeSeedTokenContract.grantRole(MINTER_ROLE, swapContract.address)
        await eeSeedTokenContract.grantRole(DEFAULT_ADMIN_ROLE, ADMIN)

        if (network === 'live_mainnet') {
            await eeSeedTokenContract.revokeRole(MINTER_ROLE, accounts[0])
            await eeSeedTokenContract.revokeRole(DEFAULT_ADMIN_ROLE, accounts[0])
        }
    } catch (err) {
        console.error(err)
    }
}