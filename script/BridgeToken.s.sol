// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import "forge-std/Script.sol";

import {L7LToken} from "src/token/L7LToken.sol";
import {SendParam, MessagingFee} from "@layerzerolabs/lz-evm-oapp-v2/contracts/oft/interfaces/IOFT.sol";

contract BridgeTokenScript is Script {
    L7LToken internal token;

    function setUp() public {}

    function run() public {
        address tokenAddress = vm.envAddress("TOKEN");

        vm.startBroadcast(msg.sender);
        token = L7LToken(tokenAddress);
        token.send{value: 167006032193920}(
            SendParam(
                30109,
                0x00000000000000000000000084a431bd2c958414b2e316cedd9f85993ace5000,
                1000000000000000000,
                1000000000000000000,
                hex"00030100110100000000000000000000000000030d40",
                "",
                ""
            ),
            MessagingFee(167006032193920, 0),
            msg.sender
        );

        vm.stopBroadcast();
    }
}
