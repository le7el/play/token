// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import "forge-std/Script.sol";

import {BridgedL7LToken} from "src/token/BridgedL7LToken.sol";

contract DeployBridgedL7LScript is Script {
    BridgedL7LToken internal token;

    address constant public POLYGON_L0_ENDPOINT = 0x1a44076050125825900e736c501f859c50fE728c;

    function setUp() public {}

    function run() public {
        address admin = vm.envAddress("ADMIN");

        vm.startBroadcast(msg.sender);
        token = new BridgedL7LToken(POLYGON_L0_ENDPOINT, admin);
        console.log("Bridged L7L token: ");
        console.log(address(token));
        vm.stopBroadcast();
    }
}
