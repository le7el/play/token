// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import "forge-std/Script.sol";

import {IListing} from "src/interfaces/IListing.sol";
import {Shop} from "src/swap/Shop.sol";
import {IDOPaymentManager} from "src/swap/shop/IDOPaymentManager.sol";
import {EESeedDeliveryManager} from "src/swap/shop/EESeedDeliveryManager.sol";
import {IERC721} from "@openzeppelin/contracts/token/ERC721/IERC721.sol";

contract DeployIDOShopScript is Script {
    Shop internal shop;
    IDOPaymentManager internal paymentManager;
    EESeedDeliveryManager internal deliveryManager;

    function setUp() public {}

    function run() public {
        vm.startBroadcast(msg.sender);
        address admin = vm.envAddress("ADMIN");
        address beneficiary = vm.envAddress("BENEFICIARY");
        address discountNFTAddress = vm.envAddress("DISCOUNT_NFT");
        address seedTokenAddress = vm.envAddress("SEED_TOKEN");
        address wethTokenAddress = vm.envAddress("WETH_ADDRESS");
        address usdcTokenAddress = vm.envAddress("USDC_ADDRESS");
        address usdtTokenAddress = vm.envAddress("USDT_ADDRESS");

        shop = new Shop(beneficiary, admin);
        console.log("Shop: ");
        console.log(address(shop));

        paymentManager = new IDOPaymentManager(address(shop), IERC721(discountNFTAddress), 92);
        console.log("IDO Payment Manager: ");
        console.log(address(paymentManager));

        shop.adminChangeDefaultPaymentManager(paymentManager);
        shop.grantRole(shop.SELLER_ROLE(), admin);

        // Comment the section below in prod
        deliveryManager = new EESeedDeliveryManager(admin, address(shop));
        console.log("EESeed Delivery Manager: ");
        console.log(address(deliveryManager));

        shop.list(
            keccak256("SEED_EE_POL_SALE"),
            IListing.PaymentMethod(
                0x0000000000000000000000000000000000000000,
                0,
                IListing.PaymentType.GAS_COIN_TRANSFER,
                beneficiary,
                address(paymentManager)
            ),
            IListing.Ware(
                seedTokenAddress,
                0,
                IListing.WareType.ERC20_MINT,
                4571428 ether,
                1000,       // increase add 3 decimal precision
                20045,      // 25.057 (100% without airdrop), (10^18/(0.0175/0.4385*10^18))
                address(deliveryManager)
            )
        );

        shop.list(
            keccak256("SEED_EE_WETH_SALE"),
            IListing.PaymentMethod(
                wethTokenAddress,
                0,
                IListing.PaymentType.ERC20_TRANSFER,
                beneficiary,
                address(paymentManager)
            ),
            IListing.Ware(
                seedTokenAddress,
                0,
                IListing.WareType.ERC20_MINT,
                4571428 ether,
                1,
                144000, // 180000 (100% without airdrop), (10^18/(0.0175/3150*10^18))
                address(deliveryManager)
            )
        );

        shop.list(
            keccak256("SEED_EE_USDC_SALE"),
            IListing.PaymentMethod(
                usdcTokenAddress,
                0,
                IListing.PaymentType.ERC20_TRANSFER,
                beneficiary,
                address(paymentManager)
            ),
            IListing.Ware(
                seedTokenAddress,
                0,
                IListing.WareType.ERC20_MINT,
                4571428 ether,
                1,
                45714285714285,  // 57142857142857 (100% without airdrop), 10^18 / (0.0175 * 1000000)
                address(deliveryManager)
            )
        );

        shop.list(
            keccak256("SEED_EE_USDT_SALE"),
            IListing.PaymentMethod(
                usdtTokenAddress,
                0,
                IListing.PaymentType.ERC20_TRANSFER,
                beneficiary,
                address(paymentManager)
            ),
            IListing.Ware(
                seedTokenAddress,
                0,
                IListing.WareType.ERC20_MINT,
                4571428 ether,
                1,
                45714285714285, // 57142857142857 (100% without airdrop), 10^18 / (0.0175 * 1000000)
                address(deliveryManager)
            )
        );

        deliveryManager.adminAuthorizeWareListing(keccak256("SEED_EE_POL_SALE"), seedTokenAddress);
        deliveryManager.adminAuthorizeWareListing(keccak256("SEED_EE_WETH_SALE"), seedTokenAddress);
        deliveryManager.adminAuthorizeWareListing(keccak256("SEED_EE_USDC_SALE"), seedTokenAddress);
        deliveryManager.adminAuthorizeWareListing(keccak256("SEED_EE_USDT_SALE"), seedTokenAddress);

        vm.stopBroadcast();
    }
}
