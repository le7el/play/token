// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

import 'forge-std/Script.sol';

import {L7InvestorsSnapshot} from 'src/swap/shop/L7InvestorsSnapshot.sol';

contract DeployL7InvestorSnapshotScript is Script {

    function setUp() public {}

    function run() public {
        vm.startBroadcast(msg.sender);
        L7InvestorsSnapshot snapshot = new L7InvestorsSnapshot();
        console.log('L7 Investors snapshot: ');
        console.log(address(snapshot));

        vm.stopBroadcast();
    }
}
