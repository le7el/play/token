// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import "forge-std/Script.sol";

import {MultiMinter} from "src/MultiMinter.sol";
import {L7LToken} from "src/token/L7LToken.sol";
import {IMinter} from "src/interfaces/IMinter.sol";
import {IERC20} from "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract DeployL7LScript is Script {
    L7LToken internal token;
    MultiMinter internal minter;
 
    address constant public SEPOLIA_TESTNET_L0_ENDPOINT = 0x6EDCE65403992e310A62460808c4b910D972f10f;
    address constant public ETHEREUM_L0_ENDPOINT = 0x1a44076050125825900e736c501f859c50fE728c;
    address constant public POLYGON_L0_ENDPOINT = 0x1a44076050125825900e736c501f859c50fE728c;

    function setUp() public {}

    function run() public {
        address admin = vm.envAddress("ADMIN");

        vm.startBroadcast(msg.sender);
        if (block.chainid == 11155111) {
            token = new L7LToken(SEPOLIA_TESTNET_L0_ENDPOINT, admin);
        } else if (block.chainid == 137) {
            token = new L7LToken(POLYGON_L0_ENDPOINT, admin);
        } else if (block.chainid == 1) {
            token = new L7LToken(ETHEREUM_L0_ENDPOINT, admin);
        }
        console.log("L7L token: ");
        console.log(address(token));

        minter = new MultiMinter(msg.sender, IMinter(address(token)));
        console.log("MultiMinter: ");
        console.log(address(minter));

        token.transferMinter(address(minter));
        minter.adminAcceptMinter();
        minter.transferOwnership(admin);

        vm.stopBroadcast();
    }
}
