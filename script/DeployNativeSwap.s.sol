// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

import "forge-std/Script.sol";

import {NativeSwap} from "src/swap/NativeSwap.sol";
import {EESeedToken} from "src/token/EESeedToken.sol";

contract DeployNativeSwapScript is Script {
    NativeSwap internal swap;

    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
    bytes32 public constant DEFAULT_ADMIN_ROLE = 0x00;

    uint internal EXCHANGE_RATE = 350;
    uint internal EXCHANGE_BASE = 10000 * 2070;
    uint internal BONUS_EXCHANGE_RATE = 324;
    bytes internal KYC_SIGNER = abi.encode(0xE20547451D486c7D0223AEEa2Cc2BB56598e5D7b);

    EESeedToken internal token;

    function setUp() public {}

    function run() public {
        vm.startBroadcast(msg.sender);

        address admin = vm.envAddress("ADMIN");
        address beneficiary = vm.envAddress("BENEFICIARY");
        address vendor_token = vm.envAddress("VENDOR_TOKEN");

        token = EESeedToken(vendor_token);

        swap = new NativeSwap(
            admin,
            payable(beneficiary),
            vendor_token,
            EXCHANGE_RATE,
            EXCHANGE_BASE,
            BONUS_EXCHANGE_RATE,
            KYC_SIGNER
        );
        console.log("Swap address: ");
        console.log(address(swap));

        //token.grantRole(MINTER_ROLE, address(swap));
        //token.grantRole(DEFAULT_ADMIN_ROLE, admin);
        //console.log("Token rights granted");

        vm.stopBroadcast();
    }
}
