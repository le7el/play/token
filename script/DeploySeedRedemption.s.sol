// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import "forge-std/Script.sol";

import {RedeemSeedByMint} from "src/vesting/RedeemSeedByMint.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract DeploySeedRedemptionScript is Script {
    RedeemSeedByMint internal redeemer;
 
    function setUp() public {}

    function run() public {
        address admin = vm.envAddress("ADMIN");
        address l7lMinter = vm.envAddress("L7L_MINTER_ADDRESS");
        address seedToken = vm.envAddress("SEED_TOKEN_ADDRESS");
        uint256 vestingStart = vm.envUint("VESTING_START");
        uint256 vestingEnd = vm.envUint("VESTING_END");

        IERC20 seed = IERC20(seedToken);
        uint256 mintCap = seed.totalSupply();

        vm.startBroadcast(msg.sender);
        redeemer = new RedeemSeedByMint(
            admin,
            IERC20(seedToken),
            IERC20(l7lMinter),
            1,
            1,
            vestingStart,
            vestingEnd,
            mintCap
        );
        console.log("RedeemSeedByMint: ");
        console.log(address(redeemer));

        vm.stopBroadcast();
    }
}
