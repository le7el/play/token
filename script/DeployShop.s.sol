// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import "forge-std/Script.sol";

import {IListing} from "src/interfaces/IListing.sol";
import {Shop} from "src/swap/Shop.sol";
import {PerkManager} from "src/swap/shop/PerkManager.sol";
import {PaymentManager} from "src/swap/shop/PaymentManager.sol";
import {PerkDeliveryManager} from "src/swap/shop/PerkDeliveryManager.sol";
import {EncodeUtils} from "../lib/generative-art/src/EncodeUtils.sol";

contract DeployShopScript is Script {
    Shop internal shop;
    PerkManager internal perkManager;
    PaymentManager internal paymentManager;
    PerkDeliveryManager internal deliveryManager;

    function setUp() public {}

    function run() public {
        vm.startBroadcast(msg.sender);
        address admin = vm.envAddress("ADMIN");
        address beneficiary = vm.envAddress("BENEFICIARY");

        shop = new Shop(admin, beneficiary);
        console.log("Shop: ");
        console.log(address(shop));

        perkManager = new PerkManager();
        console.log("Perk Manager: ");
        console.log(address(perkManager));

        paymentManager = new PaymentManager(address(shop));
        console.log("Payment Manager: ");
        console.log(address(paymentManager));

        shop.adminChangeDefaultPaymentManager(paymentManager);
        shop.grantRole(shop.SELLER_ROLE(), admin);

        // Comment the section below in prod
        deliveryManager = new PerkDeliveryManager(admin, address(shop));
        console.log("Delivery Manager: ");
        console.log(address(deliveryManager));

        // The New Order WL related code
        bytes memory _metadata = abi.encodePacked(
            "data:application/json;base64,",
            EncodeUtils.base64(bytes("{\"name\":\"The New Order WL\",\"description\":\"WL spot for the Next-Gen Decentralized Shooter powered by Arena-Z\",\"image\":\"https://ipfs.le7el.com/ipfs/QmXdUS32RVcMBiiwX5kZaHHuhmomgh4pjr6hH3YyxPHigD\",\"attributes\":[{\"trait_type\":\"Type\",\"value\":\"WL\"}]}"))
        );

        uint256 testPerk = perkManager.registerPerk(admin, 100000, true, _metadata);
        perkManager.togglePerkManager(testPerk, address(deliveryManager), true);

        shop.list(
            keccak256("TNO_WL"),
            IListing.PaymentMethod(
                0x3fd6C4A1E22f582953fA80CA229E6042B8A9ffa4,
                1,
                IListing.PaymentType.ERC1155_HOLD,
                0x0000000000000000000000000000000000000000,
                0x0000000000000000000000000000000000000000
            ),
            IListing.Ware(
                address(perkManager),
                testPerk,
                IListing.WareType.PERK_EXCLUSIVE,
                100000,
                250,
                1,
                address(deliveryManager)
            )
        );

        deliveryManager.adminAuthorizeWareListing(keccak256("TNO_WL"), address(perkManager));

        vm.stopBroadcast();
    }
}
