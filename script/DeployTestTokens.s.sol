// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.17;

import "forge-std/Script.sol";

import {PaymentTokenMock} from "src/mocks/PaymentTokenMock.sol";

contract DeployTestTokensScript is Script {
    PaymentTokenMock internal token;

    function setUp() public {}

    function run() public {
        vm.startBroadcast(msg.sender);
        token = new PaymentTokenMock(msg.sender);
        console.log("Payment token: ");
        console.log(address(token));

        vm.stopBroadcast();
    }
}
