// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import "forge-std/Script.sol";

import {PerkManager} from "src/swap/shop/PerkManager.sol";
import {EncodeUtils} from "../lib/generative-art/src/EncodeUtils.sol";

contract DeployUpdatePerkMetadataScript is Script {
    PerkManager internal perkManager;

    function setUp() public {}

    function run() public {
        vm.startBroadcast(msg.sender);
        perkManager = PerkManager(0xB2a6B7dC13E8545cb035464F53Cf7D4ac8a408E5);

        // The New Order WL related code
        // bytes memory _metadata = abi.encodePacked(
        //     "data:application/json;base64,",
        //     EncodeUtils.base64(bytes("{\"name\":\"The New Order WL\",\"description\":\"WL spot for the Next-Gen Decentralized Shooter powered by Arena-Z\",\"image\":\"https://ipfs.le7el.com/ipfs/QmXdUS32RVcMBiiwX5kZaHHuhmomgh4pjr6hH3YyxPHigD\",\"decimals\":0,\"attributes\":[{\"trait_type\":\"Type\",\"value\":\"WL\"}],\"properties\":{\"collection\":\"The New Order\",\"category\":\"Whitelist\"},\"external_url\":\"https://arena-z.gg/gamesdetail/TNO\",\"background_color\":\"FFFFFF\"}"))
        // );

        bytes memory _metadata = abi.encodePacked("https://tno-perk-metadata.summer-math-cb88.workers.dev");
        perkManager.updatePerk(1, 0x1710476Dc31c9Ee497556d31634Fc4b1Ded07f9C, 100000, true, _metadata);

        vm.stopBroadcast();
    }
}
