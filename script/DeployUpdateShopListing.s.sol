// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import "forge-std/Script.sol";

import {IListing} from "src/interfaces/IListing.sol";
import {Shop} from "src/swap/Shop.sol";
import {PerkManager} from "src/swap/shop/PerkManager.sol";
import {GDXPaymentManager} from "src/swap/shop/GDXPaymentManager.sol";
import {PerkDeliveryManager} from "src/swap/shop/PerkDeliveryManager.sol";
import {EncodeUtils} from "../lib/generative-art/src/EncodeUtils.sol";

contract DeployUpdateShopListingScript is Script {
    Shop internal shop;
    PerkManager internal perkManager;
    GDXPaymentManager internal paymentManager;
    PerkDeliveryManager internal deliveryManager;

    function setUp() public {}

    function run() public {
        vm.startBroadcast(msg.sender);
        shop = Shop(0x1B60e4A71f91590Db733aDe6A1b72F8D50d5fc73);

        // shop.delist(keccak256("TNO_WL"));

        // shop.list(
        //     keccak256("TNO_GDX_WL"),
        //     IListing.PaymentMethod(
        //         0x3fd6C4A1E22f582953fA80CA229E6042B8A9ffa4,
        //         1,
        //         IListing.PaymentType.ERC1155,
        //         0x0000000000000000000000000000000000000000,
        //         0x0000000000000000000000000000000000000000
        //     ),
        //     IListing.Ware(
        //         0xeA46Bf7Fe6c9a2e29a97D2Da2775131067b4BA0C,
        //         1,
        //         IListing.WareType.PERK,
        //         100000,
        //         250,
        //         1,
        //         0xF59F671789FB2204b87f6c4405CDf2A6cdA585AA
        //     )
        // );

        // paymentManager = new GDXPaymentManager(0x1B60e4A71f91590Db733aDe6A1b72F8D50d5fc73);

        shop.updateListing(
            keccak256("TNO_WL"),
            0x1710476Dc31c9Ee497556d31634Fc4b1Ded07f9C,
            IListing.PaymentMethod(
                0x3fd6C4A1E22f582953fA80CA229E6042B8A9ffa4,
                1,
                IListing.PaymentType.ERC1155_BURN,
                0x0000000000000000000000000000000000000000,
                0x4B08725B4EdD7a41924888204798FFA14e85c41c
            ),
            IListing.Ware(
                0xB2a6B7dC13E8545cb035464F53Cf7D4ac8a408E5,
                1,
                IListing.WareType.PERK,
                100000,
                250,
                1,
                0x2e19001639A09356b70C7c7ccF5c1f38950f1BE0
            )
        );

        vm.stopBroadcast();
    }
}
