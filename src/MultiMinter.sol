// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import { Pausable } from "@openzeppelin/contracts/utils/Pausable.sol";
import { Ownable } from "@openzeppelin/contracts/access/Ownable.sol";
import { Ownable2Step } from "@openzeppelin/contracts/access/Ownable2Step.sol";
import { IMinter } from "./interfaces/IMinter.sol";
import { IMintingRights } from "./interfaces/IMintingRights.sol";

/**
 * @dev Adapted version of Aragon's multi-minter smart contract
 *      ref. https://github.com/aragon/aragon-network-token/blob/master/packages/v2/contracts/ANTv2MultiMinter.sol
 *      added more solid ownership controls and safeguards.
 */
contract MultiMinter is Ownable2Step, Pausable, IMinter {
    IMinter public mintable;

    mapping (address => bool) public canMint;

    event AddedMinter(address indexed minter);
    event RemovedMinter(address indexed minter);

    error NotMinter();

    modifier onlyMinter() {
        if (!canMint[msg.sender] && msg.sender != owner()) revert NotMinter();
        _;
    }

    /**
     * @dev Setup custom owner and mintable token.
     *
     * @param _owner Owner of minter, who can manage minters.
     * @param _mintable Token address which can be minted.
     */
    constructor(address _owner, IMinter _mintable) Ownable(_owner) {
        mintable = _mintable;
    }

    /**
     * @dev Proxies balance to underlying mintable token.
     *
     * @param _account Owner of balance.
     */
    function balanceOf(address _account) external view returns (uint256) {
        return mintable.balanceOf(_account);
    }

    /**
     * @dev Mint amount of tokens to the address for ERC1155 multi-token standard.
     *
     * @param _to Address of token reciever.
     * @param _amount Amount of minted tokens.
     */
    function mint(address _to,uint256 _amount) external whenNotPaused onlyMinter {
        mintable.mint(_to, _amount);
    }

    /**
     * @dev Enable minting rights for some address.
     *
     * @param _minter Address of a new minter.
     */
    function addMinter(address _minter) external onlyOwner {
        canMint[_minter] = true;
        emit AddedMinter(_minter);
    }

    /**
     * @dev Disable minting rights for some address.
     *
     * @param _minter Address of a current minter.
     */
    function removeMinter(address _minter) external onlyOwner {
        canMint[_minter] = false;
        emit RemovedMinter(_minter);
    }

    /**
     * @dev Accept ownership of external smart contract using 2-step verification
     */
    function adminAcceptMinter() external onlyOwner {
        IMintingRights(address(mintable)).acceptMinter();
    }

    /**
     * @dev Change address of minter on the token contract.
     *
     * @param _newMinter Address of a new minter contract or wallet.
     */
    function adminMigrate(address _newMinter) external onlyOwner {
        IMintingRights(address(mintable)).transferMinter(_newMinter);
    }

    /**
     * @dev Pauses all mints.
     */
    function adminPause() public onlyOwner {
        _pause();
    }

    /**
     * @dev Unpauses all mints.
     */
    function adminUnpause() public onlyOwner {
        _unpause();
    }
}