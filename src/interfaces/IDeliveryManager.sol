// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import { IListing } from "./IListing.sol";

/**
 * @dev Interface of a DeliveryManager.
 */
interface IDeliveryManager is IListing {
    error Unauthorized();
    error UnsupportedWareType();

    event WareAuthorizationGranted(
        address indexed admin,
        address indexed wareAddress
    );

    event WareAuthorizationRevoked(
        address indexed admin,
        address indexed wareAddress
    );

    event ListingAuthorizationGranted(
        address indexed admin,
        bytes32 indexed listingUid
    );

    event ListingAuthorizationRevoked(
        address indexed admin,
        bytes32 indexed listingUid
    );

    event WareDelivered(
        bytes32 indexed listingUid,
        bytes32 indexed wareUid,
        address indexed buyer,
        WareType wareType,
        address wareAddress,
        uint256 wareId,
        uint256 purchaseUnits,
        bytes context
    );

    /**
     * @dev Delivers bought ware, it's expected that payments and all the validations were handled earlier on SHOP contract level.
     *
     * @param _uid Unique identifier of listing.
     * @param _listing Structure with listing data.
     * @param _buyer Address which would get the delivery.
     * @param _purchaseUnits Amount of ware units to be delivered.
     * @param _context Additional data like specific NFT id.
     */
    function deliver(bytes32 _uid, Listing memory _listing, address _buyer, uint256 _purchaseUnits, bytes memory _context) external;
}