// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Interface of ERC1155 token with standard burning feature.
 */
interface IERC1155Burnable {
    /**
     * @dev Burn token by id from account, according to approval limit.
     *
     * @param account Account address to burn from.
     * @param id Token id to burn.
     * @param value Amount of tokens to burn.
     */
    function burn(address account, uint256 id, uint256 value) external;
}
