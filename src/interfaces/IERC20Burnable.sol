// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Interface of ERC20 token with standard burning feature.
 */
interface IERC20Burnable {
    /**
     * @dev Burn tokens from account, according to approval limit.
     * 
     * @param account Account address to burn from.
     * @param value Amount of tokens to burn.
     */
    function burnFrom(address account, uint256 value) external;
}
