// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Interface of ERC721 token with burningFrom feature, default burning doesn't support burning by approval.
 */
interface IERC721BurnableFrom {
    /**
     * @dev Burn tokens from account, according to approval limit.
     * 
     * @param account Account address to burn from.
     * @param tokenId Id of token to burn.
     */
    function burnFrom(address account, uint256 tokenId) external;
}
