// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

/**
 * @dev Defines Listing structure with Ware and PaymentMethod information.
 */
interface IListing {
    enum WareType{
        PERK,
        PERK_EXCLUSIVE,
        ERC20_TRANSFER,
        ERC1155_TRANSFER,
        ERC721_TRANSFER,
        ERC20_MINT,
        ERC1155_MINT,
        ERC721_MINT,
        GAS_COIN_TRANSFER // Most likely, you don't need this one
    }
    enum PaymentType{
        PERK_REVOKE,
        PERK_USE,
        ERC20_TRANSFER,
        ERC20_BURN,
        ERC20_HOLD, // Don't use with non-soulbonded tokens
        ERC1155_TRANSFER,
        ERC1155_BURN,
        ERC1155_HOLD, // Don't use with non-soulbonded tokens
        ERC721_TRANSFER,
        ERC721_BURN,
        ERC721_HOLD,
        GAS_COIN_TRANSFER
    }

    struct PaymentMethod {
        // Address of token (ERC20, ERC1155 or ERC721) to spend. PerkManager contract is also supported.
        address tokenAddress;
        // Token id to spend, use 0 for ERC20, for ERC721 0 would allow using any NFT id. Perk id is also supported.
        uint256 tokenId;
        // Check PaymentType.
        PaymentType paymentType;
        // Address which would recieve spent tokens, use BURN_ADDRESS for burning.
        address beneficiary;
        // Smart contract which handles the payment part (can grant a discount, call non-standard methods etc).
        address paymentManager;
    }

    struct Ware {
        // Address of token (ERC20, ERC1155 or ERC721) to recieve. PerkManager contract is also supported.
        address wareAddress;
        // Ware id to spend, use 0 for ERC20, for ERC721 0 would allow using any NFT id. Perk id is also supported.
        uint256 wareId;
        // Check WareTypes.
        WareType wareType;
        // Maximum amount of ware units to be sold.
        uint256 inventoryAmount;
        // Price per unit in PaymentMethod token, 0 for a free reward.
        uint256 pricePerUnit;
        // Divisor is used to allow exchange of more expensive token to a cheaper ones.
        uint256 pricePerUnitDivisor;
        // Smart contract address which is called to deliver the ware, defaults are not supported.
        address deliveryManager;
    }

    struct Listing {
        // Admin address of this listing.
        address seller;
        // See PaymentMethod structure.
        PaymentMethod paymentMethod;
        // See Ware structure.
        Ware ware;
    }

    event AssetListed(
        bytes32 indexed uid,
        address indexed seller,
        bytes32 indexed wareUid,
        PaymentMethod paymentMethod,
        Ware ware
    );

    event AssetDelisted(
        bytes32 indexed uid,
        address indexed seller,
        bytes32 indexed wareUid
    );

    event AssetListingUpdated(
        bytes32 indexed uid,
        address indexed seller,
        bytes32 indexed wareUid,
        address newSeller,
        PaymentMethod paymentMethod,
        Ware ware
    );

    event AssetExchanged(
        bytes32 indexed uid,
        address indexed buyer,
        bytes32 indexed wareUid,
        PaymentMethod paymentMethod,
        Ware ware,
        uint256 purchaseUnits,
        uint256 price
    );
}