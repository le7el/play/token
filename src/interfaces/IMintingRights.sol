// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Transferable minting rights with 2-step verification
 */
interface IMintingRights {
    /**
     * @dev New minter address.
     */
    function transferMinter(address newMinter) external;

    /**
     * @dev Accept external minting rights transfer.
     */
    function acceptMinter() external;
}