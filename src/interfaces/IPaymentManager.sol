// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import { IListing } from "./IListing.sol";

/**
 * @dev Interface of the PaymentManager.
 */
interface IPaymentManager is IListing {
    error Unauthorized();
    error PaymentFailed();
    error UnsupportedExchange();
    error InvalidConfig();

    event WareBought(
        bytes32 indexed listingUid,
        bytes32 indexed wareUid,
        address indexed payer,
        PaymentType paymentType,
        address tokenAddress,
        uint256 tokenId,
        uint256 purchaseUnits,
        uint256 price,
        bytes context
    );

    /**
     * @dev Process payment for sold ware, can implement discounts and other custom features.
     *
     * @param _uid Unique identifier of listing.
     * @param _listing Structure with listing data.
     * @param _payer Address which pay for the ware in listing.
     * @param _purchaseUnits Amount of ware units to be bought.
     * @param _context Additional data like specific NFT id.
     */
    function payFor(
        bytes32 _uid,
        Listing memory _listing,
        address _payer,
        uint256 _purchaseUnits,
        bytes memory _context
    ) external payable;

    /**
     * @dev Get a quote which should return price and payment token information.
     *
     * @param _payer Address which pay for the ware in listing.
     * @param _listing Structure with listing data.
     * @param _purchaseUnits Amount of ware units to be bought.
     * @param _context Additional data like specific NFT id.
     * @return Amount in quote token.
     * @return Quote token address.
     * @return Quote token id. 
     */
    function quoteFor(
        address _payer,
        Listing memory _listing,
        uint256 _purchaseUnits,
        bytes memory _context
    ) external view returns (uint256, address, uint256);
}