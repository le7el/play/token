// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

/**
 * @dev Interface of the PerkManager.
 */
interface IPerkManager {
    /**
     * @dev Return perk's metadata.
     *
     * @param _id Unique id of perk.
     */
    function uri(uint256 _id) external view returns (string memory);

    /**
     * @dev Amount of a specific perk owned by the user.
     *
     * @param _user Address which owns the perk.
     * @param _id Unique id of perk.
     * @return amount of perks owned by the user.
     */
    function balanceOf(address _user, uint256 _id) external view returns (uint256);

    /**
     * @dev Get batched perk balances for each user.
     *
     * @param _users Addresses which own perks.
     * @param _ids Unique ids of perks.
     * @return array with balances.
     */
    function balanceOfBatch(address[] calldata _users, uint256[] calldata _ids) external view returns (uint256[] memory);

    /**
     * @dev Check how many times perk was used in total for all usages.
     *
     * @param _id Unique id of perk.
     * @param _user Address which used the perk.
     * @return count of total usages.
     */
    function perkTotalUsages(uint256 _id, address _user) external view returns (uint256);

    /**
     * @dev Check how many times perk was used in scope of usage uid.
     *
     * @param _id Unique id of perk.
     * @param _user Address which used the perk.
     * @param _usageUid Unique identifier to prevent double usage.
     * @return count of usages.
     */
    function perkUsages(uint256 _id, address _user, bytes32 _usageUid) external view returns (uint256);

    /**
     * @dev List new on-chain perk.
     *
     * @param _owner Address which can change perk's settings.
     * @param _totalSupply Max amount of perks to be distributed.
     * @param _exclusive If true, only one perk can be granted to a user.
     * @param _metadata Perk's metadata.
     * @return registered perk id.
     */
    function registerPerk(address _owner, uint256 _totalSupply, bool _exclusive, bytes memory _metadata) external returns (uint256);

    /**
     * @dev Update existing on-chain perk.
     *
     * @param _id Perk id to update.
     * @param _newOwner Address which can change perk's settings.
     * @param _totalSupply Max amount of perks to be distributed.
     * @param _exclusiveOff Can make exlisive perk non exclusive.
     * @param _metadata Perk's metadata.
     */
    function updatePerk(uint256 _id, address _newOwner, uint256 _totalSupply, bool _exclusiveOff, bytes memory _metadata) external;

    /**
     * @dev Give on-chain perk to the user.
     *
     * @param _id Unique id of perk.
     * @param _user Address which should get the perk.
     * @param _amount Amount of perks to be granted.
     */
    function grantPerk(uint256 _id, address _user, uint256 _amount) external;

    /**
     * @dev Revoke on-chain perk from the user. 
     *      Owner of the perk, or it's manager can do the revokation.
     *
     * @param _id Id of perk.
     * @param _user Address from whick perk is revoked.
     * @param _amount Amount of perks to be revoked.
     */
    function revokePerk(uint256 _id, address _user, uint256 _amount) external;

    /**
     * @dev Register usage of perk in scope of some usage id.
     *      Usually it's used for redemption of extra rewards.
     *
     * @param _id Id of perk.
     * @param _user Address from whick perk is used.
     * @param _usageUid Unique identifier to prevent double usage.
     * @return count of usages in scope of _usageUid.
     * @return total count of usages.
     */
    function usePerk(uint256 _id, address _user, bytes32 _usageUid) external returns (uint256, uint256);
}