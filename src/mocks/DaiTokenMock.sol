// SPDX-License-Identifier: MPL-2.0

pragma solidity ^0.8.0;

import "../../node_modules/@openzeppelin/contracts/token/ERC20/ERC20.sol";

/** 
 * @title Mockup for payment token.
 */
contract DaiTokenMock is ERC20 {
    constructor(address _creditor) ERC20("DAI mock", "mockDAI") {
        _mint(_creditor, 10000000 ether);
    }

    /**
     * @dev Get some payment tokens for testing.
     */
    function faucet() external {
        _mint(msg.sender, 250000 ether);
    }
}