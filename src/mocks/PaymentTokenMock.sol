// SPDX-License-Identifier: MPL-2.0

pragma solidity ^0.8.0;

import "../../node_modules/@openzeppelin/contracts/token/ERC20/ERC20.sol";

/** 
 * @title Mockup for payment token.
 */
contract PaymentTokenMock is ERC20 {
    constructor(address _creditor) ERC20("USD mock", "mockUSD") {
        _mint(_creditor, 10000000000000);
    }

    /**
     * @dev Same as USDC.
     */
    function decimals() public pure override returns (uint8) {
        return 6;
    }

    /**
     * @dev Get some payment tokens for testing.
     */
    function faucet() external {
        _mint(msg.sender, 250000000000);
    }
}