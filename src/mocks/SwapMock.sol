// SPDX-License-Identifier: MPL-2.0

pragma solidity ^0.8.0;

import "../swap/Swap.sol";

/** 
 * @title Mockup for swap to keep consistent external signatures.
 */
contract SwapMock is Swap {
    constructor(
        address _admin,
        address _beneficiary,
        address _vendorToken,
        address _paymentToken,
        uint _exchangeRate,
        uint _exchangeBase,
        uint _bonusExchangeRate,
        bytes memory _kycSignerData
    ) Swap(
        _admin,
        _beneficiary,
        _vendorToken,
        _paymentToken,
        _exchangeRate,
        _exchangeBase,
        _bonusExchangeRate,
        _kycSignerData
    ) {}

    /**
     * @dev Use static BENEFICIARY instead of address(this) for deterministic testing.
     *
     * @return EIP-712 domain.
     */
    function getDomainSeparator() public override view returns(bytes32) {
        // NOTE: Currently, the only way to get the chain ID in solidity is
        // using assembly.
        uint256 chainId;
        // solhint-disable-next-line no-inline-assembly
        assembly {
            chainId := chainid()
        }

        return keccak256(
            abi.encode(
                DOMAIN_TYPE_HASH,
                DOMAIN_NAME,
                DOMAIN_VERSION,
                chainId,
                BENEFICIARY
            )
        );
    }
}