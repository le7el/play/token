// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import { ReentrancyGuard } from "@openzeppelin/contracts/utils/ReentrancyGuard.sol";
import { Pausable } from "@openzeppelin/contracts/utils/Pausable.sol";
import { AccessControl } from "@openzeppelin/contracts/access/AccessControl.sol";
import { IListing } from "../interfaces/IListing.sol";
import { IPaymentManager } from "../interfaces/IPaymentManager.sol";
import { IDeliveryManager } from "../interfaces/IDeliveryManager.sol";

/** 
 * @title B2C shop for selling on-chain assets and perks.
 */
contract Shop is IListing, AccessControl, Pausable, ReentrancyGuard {
    bytes32 public constant SELLER_ROLE = keccak256("SELLER_ROLE");

    mapping(bytes32 => Listing) public listings;

    address public beneficiary;
    IPaymentManager public defaultPaymentManager;

    bool public restrictedListing = true;
    mapping(bytes32 => uint256) public customFeeListings;
    uint256 public listingFee;
    uint256 public exchangeFee;

    error Unauthorized();
    error InvalidListing();
    error ComissionFailed();
    error PaymentFailed();
    error OutOfStock();
    error InvalidPrice();
    error InvalidPurchase();

    event NewBeneficiary(address indexed admin, address beneficiary);
    event PublicListingEnabled(address indexed admin);
    event PublicListingDisabled(address indexed admin);
    event ListingFeeChanged(address indexed admin, uint256 fee);
    event ExchangeFeeChanged(address indexed admin, uint256 fee);
    event ListingCustomFeeChanged(address indexed admin, bytes32 indexed listingUid, uint256 fee);
    event DefaultPaymentManagerChanged(address indexed admin, address paymentManager);

    modifier isSellerIfRestricted() {
        if (restrictedListing && !hasRole(SELLER_ROLE, _msgSender())) revert Unauthorized();
        _;
    }

    /**
     * @dev Configure shop. 
     *      Admin can use grantRole and revokeRole to manager SELLER_ROLE.
     *
     * @param _beneficiary Address which would recieve global procceds from trading and listing fees.
     * @param _admin Address which can change settings of this contract.
     */
    constructor(address _beneficiary, address _admin) {
        beneficiary = _beneficiary;
        _setRoleAdmin(SELLER_ROLE, DEFAULT_ADMIN_ROLE);
        _grantRole(DEFAULT_ADMIN_ROLE, _admin);
        emit NewBeneficiary(_msgSender(), _beneficiary);
    }

    /**
     * @dev Admin can pause and unpause trades and new listings.
     *
     * @param _newBeneficiary New address which would recieve listing and exchange comissions if set.
     */
    function adminChangeBeneficiary(address _newBeneficiary) external onlyRole(DEFAULT_ADMIN_ROLE) {
        if (_newBeneficiary == address(0)) revert Unauthorized();
        beneficiary = _newBeneficiary;
        emit NewBeneficiary(_msgSender(), _newBeneficiary);
    }

    /**
     * @dev Admin can pause and unpause trades and new listings.
     *
     * @param _status True to pause, false to unpause.
     */
    function adminToggleTradesAndListings(bool _status) external onlyRole(DEFAULT_ADMIN_ROLE) {
        if (_status) {
            _pause();
        } else {
            _unpause();
        }
    }

    /**
     * @dev Admin can allow unrestricted listing of wares.
     *
     * @param _status True to allow unrestricted listing, false to block it.
     */
    function adminRestrictListing(bool _status) external onlyRole(DEFAULT_ADMIN_ROLE) {
        bool _restrictedListing = restrictedListing;
        restrictedListing = _status;
        if (_status && _restrictedListing) emit PublicListingEnabled(_msgSender());
        if (!_status && !_restrictedListing) emit PublicListingDisabled(_msgSender());
    }

    /**
     * @dev Admin can set listing fee.
     *
     * @param _fee Wei units of listing fee in gas coin.
     */
    function adminSetListingFee(uint256 _fee) external onlyRole(DEFAULT_ADMIN_ROLE) {
        listingFee = _fee;
        emit ListingFeeChanged(_msgSender(), _fee);
    }

    /**
     * @dev Admin can set custom exchange fee for specific listing.
     *
     * @param _uid Listing uid to change exchange fee.
     * @param _fee Custom fee amount (1 for no fees, 0 reset to a default fee, any other amount would be customly charged).
     */
    function adminSetCustomExchangeFee(bytes32 _uid, uint256 _fee) external onlyRole(DEFAULT_ADMIN_ROLE) {
        customFeeListings[_uid] = _fee;
        emit ListingCustomFeeChanged(_msgSender(), _uid, _fee);
    }

    /**
     * @dev Admin can set exchange fee.
     *
     * @param _fee Wei units of exchange fee in gas coin.
     */
    function adminSetExchangeFee(uint256 _fee) external onlyRole(DEFAULT_ADMIN_ROLE) {
        exchangeFee = _fee;
        emit ExchangeFeeChanged(_msgSender(), _fee);
    }

    /**
     * @dev Admin can set exchange fee.
     *
     * @param _paymentManager Address of a new default payment manager contract.
     */
    function adminChangeDefaultPaymentManager(IPaymentManager _paymentManager) external onlyRole(DEFAULT_ADMIN_ROLE) {
        defaultPaymentManager = _paymentManager;
        emit DefaultPaymentManagerChanged(_msgSender(), address(_paymentManager));
    }
    
    /**
     * @dev List on-chain asset or perk.
     *
     * @param _uid Unique identifier of listing.
     * @param _paymentMethod Check PaymentMethod struct for details (defined in IListing).
     * @param _ware Check Ware struct for details (defined in IListing).
     */
    function list(
        bytes32 _uid,
        PaymentMethod memory _paymentMethod,
        Ware memory _ware
    ) external payable whenNotPaused isSellerIfRestricted nonReentrant {
        if (listings[_uid].seller != address(0)) revert InvalidListing();
        if (_ware.deliveryManager == address(0)) revert InvalidListing();

        // Comission if configured.
        uint256 _listingFee = listingFee;
        if (listingFee > 0) {
            if (msg.value < _listingFee) revert ComissionFailed();
            (bool success,) = beneficiary.call{value: msg.value}("");
            if (!success) revert ComissionFailed();
        }

        listings[_uid] = Listing(_msgSender(), _paymentMethod, _ware);
        emit AssetListed(_uid, _msgSender(), wareUid(_ware), _paymentMethod, _ware);
    }

    /**
     * @dev Seller can delist his ware, admin can delist any ware.
     *
     * @param _uid Unique identifier of listing.
     */
    function delist(bytes32 _uid) external nonReentrant {
        address _seller = listings[_uid].seller;
        if (listings[_uid].seller != _msgSender() && !hasRole(DEFAULT_ADMIN_ROLE, _msgSender())) revert Unauthorized();
        bytes32 _wareUid = wareUid(listings[_uid].ware);
        delete listings[_uid];
        emit AssetDelisted(_uid, _seller, _wareUid);
    }

    /**
     * @dev Seller can update his listing.
     *
     * @param _uid Unique identifier of listing.
     * @param _seller Can be used to change manager of listing, pass address(0) for no change.
     * @param _paymentMethod Check PaymentMethod struct for details (defined in IListing).
     * @param _ware Check Ware struct for details (defined in IListing).
     */
    function updateListing(
        bytes32 _uid,
        address _seller,
        PaymentMethod memory _paymentMethod,
        Ware memory _ware
    ) external nonReentrant {
        if (listings[_uid].seller != _msgSender()) revert Unauthorized();
        if (_ware.deliveryManager == address(0)) revert InvalidListing();

        address _newSeller = _seller;
        if (_seller == address(0)) _newSeller = listings[_uid].seller;

        listings[_uid] = Listing(_newSeller, _paymentMethod, _ware);
        emit AssetListingUpdated(_uid, _msgSender(), wareUid(_ware), _seller, _paymentMethod, _ware);
    }
    
    /**
     * @dev Exchange asset defined in PaymentMethod to asset defined in Ware for given listing.
     *
     * @param _uid Listing uid.
     * @param _bidTokenAmount Amount of tokens to be exchanged.
     * @param _purchaseUnits Amount of ware to be bought.
     * @param _context Additional context for exchange.
     */
    function exchange(
        bytes32 _uid,
        uint256 _bidTokenAmount,
        uint256 _purchaseUnits,
        bytes memory _context
    ) external payable whenNotPaused nonReentrant {
        if (_purchaseUnits == 0) revert InvalidPurchase();
        Listing memory _listing = listings[_uid];
        if (listings[_uid].seller == address(0)) revert InvalidListing();
        if (_listing.ware.inventoryAmount < _purchaseUnits) revert OutOfStock();
        (uint256 _quotePrice,,) = _quoteFor(_listing, _msgSender(), _purchaseUnits, _context);
        if (_quotePrice > _bidTokenAmount) revert InvalidPrice();

        listings[_uid].ware.inventoryAmount = _listing.ware.inventoryAmount - _purchaseUnits;

        // Comission if configured.
        uint256 _exchangeFee = customFeeListings[_uid];
        if (_exchangeFee == 0) _exchangeFee = exchangeFee;
        if (_exchangeFee > 1) {
            if (msg.value < _exchangeFee) revert ComissionFailed();
            (bool success,) = beneficiary.call{value: _exchangeFee}("");
            if (!success) revert ComissionFailed();
        }

        // Payment, it's expected that paymentManager contract should revert in case of a failed payment.
        if (_listing.paymentMethod.paymentManager != address(0)) {
            IPaymentManager(_listing.paymentMethod.paymentManager).payFor{value: msg.value - _exchangeFee}(_uid, _listing, _msgSender(), _purchaseUnits, _context);
        } else {
            IPaymentManager _defaultPaymentManager = defaultPaymentManager;
            if (address(_defaultPaymentManager) == address(0)) revert InvalidListing();
            _defaultPaymentManager.payFor(_uid, _listing, _msgSender(), _purchaseUnits, _context);
        }

        // Delivery manager should grant the perk or transfer token to the buyer, it's expected that payment was handled on a previous step.
        IDeliveryManager(_listing.ware.deliveryManager).deliver(_uid, _listing, _msgSender(), _purchaseUnits, _context);

        emit AssetExchanged(
            _uid,
            _msgSender(),
            wareUid(_listing.ware),
            _listing.paymentMethod,
            _listing.ware,
            _purchaseUnits,
            _quotePrice
        );
    }

    /**
     * @dev Check price for a listed asset.
     *
     * @param _uid Listing uid.
     * @param _buyer Address of a buyer.
     * @param _purchaseUnits Amount of ware to be bought.
     * @param _context Additional context for exchange.
     * @return Amount in quote token.
     * @return Quote token address.
     * @return Quote token id. 
     */
    function quoteFor(bytes32 _uid, address _buyer, uint256 _purchaseUnits, bytes memory _context) public view returns (uint256, address, uint256) {
        Listing memory _listing = listings[_uid];
        return _quoteFor(_listing, _buyer, _purchaseUnits, _context);
    }

    /**
     * @dev Check amount of listed asset for a given offer, reverse of quoteFor for convience.
     *
     * @param _uid Listing uid.
     * @param _buyer Address of a buyer.
     * @param _paymentTokenAmount Amount payment token to be paid.
     * @param _context Additional context for exchange.
     * @return Amount in ware token.
     * @return Payment token address.
     * @return Payment token id. 
     */
    function offerOf(bytes32 _uid, address _buyer, uint256 _paymentTokenAmount, bytes memory _context) public view returns (uint256, address, uint256) {
        Listing memory _listing = listings[_uid];
        (uint256 _pricePerDevisorUnit, address _token, uint256 _tokenId) = _quoteFor(_listing, _buyer, _listing.ware.pricePerUnitDivisor * 1 ether, _context);
        return (_paymentTokenAmount * _listing.ware.pricePerUnitDivisor * 1 ether / _pricePerDevisorUnit, _token, _tokenId);
    }

    /**
     * @dev Get unique identifier for the ware.
     *
     * @param _ware Listing ware data.
     * @return uid for the ware.
     */
    function wareUid(Ware memory _ware) public pure returns (bytes32) {
        return keccak256(abi.encodePacked(_ware.wareAddress, _ware.wareId));
    }

    /**
     * @dev Check price for a listed asset.
     *
     * @param _listing Listing data.
     * @param _buyer Address of a buyer.
     * @param _purchaseUnits Amount of ware to be bought.
     * @param _context Additional context for exchange.
     * @return Amount in quote token.
     * @return Quote token address.
     * @return Quote token id. 
     */
    function _quoteFor(
        Listing memory _listing,
        address _buyer,
        uint256 _purchaseUnits,
        bytes memory _context
    ) internal view returns (uint256, address, uint256) {
        if (_listing.seller == address(0)) revert InvalidListing();
        if (_listing.paymentMethod.paymentManager != address(0)) {
            return IPaymentManager(_listing.paymentMethod.paymentManager).quoteFor(_buyer, _listing, _purchaseUnits, _context);
        } else {
            IPaymentManager _defaultPaymentManager = defaultPaymentManager;
            if (address(_defaultPaymentManager) == address(0)) revert InvalidListing();
            return _defaultPaymentManager.quoteFor(_buyer, _listing, _purchaseUnits, _context);
        }
    }
}