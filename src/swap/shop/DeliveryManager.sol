// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import { Ownable } from "@openzeppelin/contracts/access/Ownable.sol";
import { Ownable2Step } from "@openzeppelin/contracts/access/Ownable2Step.sol";
import { IListing } from "../../interfaces/IListing.sol";
import { IDeliveryManager } from "../../interfaces/IDeliveryManager.sol";

/** 
 * @title Shared methods for DeliveryManager contracts
 *
 * @dev Each shop seller should deploy their own delivery manager, as there is always a risk of malicious
 *      wareAddress in Listing which would be able to drain this contract. For the same reason shared
 *      DeliveryManager contracts are not natively supported! 
 */
abstract contract DeliveryManager is Ownable2Step, IListing, IDeliveryManager {
    address public immutable SHOP;
    mapping(bytes32 => bool) public authorizedListings;
    mapping(address => bool) public authorizedWareAddresses;

    modifier isValidDeliveryParams(bytes32 _uid, Listing memory _listing, uint256 _purchaseUnits) {
        if (msg.sender != SHOP) revert Unauthorized();
        if (_purchaseUnits == 0) revert UnsupportedWareType();
        if (!authorizedListings[_uid]) revert Unauthorized();
        if (!authorizedWareAddresses[_listing.ware.wareAddress]) revert Unauthorized();
        _;
    }

    /**
     * @dev Configure delivery manager.
     *
     * @param _owner Address which can manage ware availability for delivery.
     * @param _shopContractAddress Trusted shop contract which can authorize the delivery.
     */
    constructor(address _owner, address _shopContractAddress) Ownable(_owner) {
        SHOP = _shopContractAddress;
    }

    /**
     * @dev Admin can authorize wareAddresses or revoke the authorization.
     *
     * @param _wareAddress Address of ware to be sold.
     * @param _status true to authorize, false to revoke authorization.
     */
    function adminToggleWareAddressAuthorization(address _wareAddress, bool _status) external onlyOwner {
        bool _currentStatus = authorizedWareAddresses[_wareAddress];
        if (_status != _currentStatus) {
            authorizedWareAddresses[_wareAddress] = _status;
            if (_status) {
                emit WareAuthorizationGranted(msg.sender, _wareAddress);
            } else {
                emit WareAuthorizationRevoked(msg.sender, _wareAddress);
            }
        }
    }

    /**
     * @dev Admin can authorize wareAddresses or revoke the authorization.
     *
     * @param _listingUid Listing uid to authorize delivery for.
     * @param _status true to authorize, false to revoke authorization.
     */
    function adminToggleListingAuthorization(bytes32 _listingUid, bool _status) external onlyOwner {
        bool _currentStatus = authorizedListings[_listingUid];
        if (_status != _currentStatus) {
            authorizedListings[_listingUid] = _status;
            if (_status) {
                emit ListingAuthorizationGranted(msg.sender, _listingUid);
            } else {
                emit ListingAuthorizationRevoked(msg.sender, _listingUid);
            }
        }
    }

    /**
     * @dev Batched admin function to authorize specific listing and related ware.
     *
     * @param _listingUid Listing uid to authorize delivery for.
     * @param _wareAddress Address of ware to be sold.
     */
    function adminAuthorizeWareListing(bytes32 _listingUid, address _wareAddress) external onlyOwner {
        bool _currentStatusListing = authorizedListings[_listingUid];
        if (!_currentStatusListing) {
            authorizedListings[_listingUid] = true;
            emit ListingAuthorizationGranted(msg.sender, _listingUid);
        }

        bool _currentStatusWare = authorizedWareAddresses[_wareAddress];
        if (!_currentStatusWare) {
            authorizedWareAddresses[_wareAddress] = true;
            emit WareAuthorizationGranted(msg.sender, _wareAddress);
        }
    }

    /**
     * @dev Delivers a bought ware, it's expected that payments and all the validations were handled earlier on a SHOP contract level.   
     *
     * @param _uid Unique identifier of listing.
     * @param _listing Structure with listing data.
     * @param _buyer Address which would get the delivery.
     * @param _purchaseUnits Amount of ware units to be delivered.
     * @param _context Additional data like specific NFT id.
     */
    function deliver(bytes32 _uid, Listing memory _listing, address _buyer, uint256 _purchaseUnits, bytes memory _context) public virtual {
        emit WareDelivered(
            _uid,
            _wareUid(_listing.ware),
            _buyer,
            _listing.ware.wareType,
            _listing.ware.wareAddress,
            _listing.ware.wareId,
            _purchaseUnits,
            _context
        );
    }

    /**
     * @dev Get unique identifier for the ware.
     *
     * @param _ware Listing ware data.
     * @return uid for the ware.
     */
    function _wareUid(Ware memory _ware) internal pure returns (bytes32) {
        return keccak256(abi.encodePacked(_ware.wareAddress, _ware.wareId));
    }
}