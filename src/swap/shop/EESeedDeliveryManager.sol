// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import { IERC20 } from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import { SafeERC20 } from "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import { IMinter } from "../../interfaces/IMinter.sol";
import { DeliveryManager } from "./DeliveryManager.sol";

/** 
 * @title Purchase SeedEE tokens through mint.
 */
contract EESeedDeliveryManager is DeliveryManager {
    using SafeERC20 for IERC20;

    /**
     * @dev Configure delivery manager.
     *
     * @param _owner Address which can manage ware availability for delivery.
     * @param _shopContractAddress Trusted shop contract which can authorize the delivery.
     */
    constructor(address _owner, address _shopContractAddress) DeliveryManager(_owner, _shopContractAddress) {}

    /**
     * @dev Delivers a bought ware, it's expected that payments and all the validations were handled earlier on a SHOP contract level.   
     *
     * @param _uid Unique identifier of listing.
     * @param _listing Structure with listing data.
     * @param _buyer Address which would get the delivery.
     * @param _purchaseUnits Amount of ware units to be delivered.
     * @param _context Additional data like specific NFT id.
     */
    function deliver(
        bytes32 _uid,
        Listing memory _listing,
        address _buyer,
        uint256 _purchaseUnits,
        bytes memory _context
    ) public override isValidDeliveryParams(_uid, _listing, _purchaseUnits) {
        if (_listing.ware.wareType != WareType.ERC20_MINT) revert UnsupportedWareType();
        IMinter(_listing.ware.wareAddress).mint(_buyer, _purchaseUnits);
        super.deliver(_uid, _listing, _buyer, _purchaseUnits, _context);
    }
}