// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import { IListing } from "../../interfaces/IListing.sol";
import { IPaymentManager } from "../../interfaces/IPaymentManager.sol";
import { IERC1155Burnable } from "../../interfaces/IERC1155Burnable.sol";

/** 
 * @title Unified interface to process payments and give individual quotes for wares bought in shop.
 */
contract GDXPaymentManager is IListing, IPaymentManager {
    address public immutable SHOP;
    address public constant GDX_TOKEN = 0x3fd6C4A1E22f582953fA80CA229E6042B8A9ffa4;

    /**
     * @dev Configure payment manager.
     *
     * @param _shopContractAddress Trusted shop contract which can authorize the payment.
     */
    constructor(address _shopContractAddress) {
        SHOP = _shopContractAddress;
    }

    /**
     * @dev Dedicated payments in GDX soulbonded token (0x3fd6C4A1E22f582953fA80CA229E6042B8A9ffa4).
     *
     * @param _uid Unique identifier of listing.
     * @param _listing Structure with listing data.
     * @param _payer Address which pay for the ware in listing.
     * @param _purchaseUnits Amount of ware units to be bought.
     * @param _context Additional data like specific NFT id.
     */
    function payFor(bytes32 _uid, Listing memory _listing, address _payer, uint256 _purchaseUnits, bytes memory _context) external payable {
        if (msg.sender != SHOP) revert Unauthorized();
        (uint256 _quotePrice, address _tokenAddress, uint256 _tokenId) = quoteFor(_payer, _listing, _purchaseUnits, _context);
        if (_listing.paymentMethod.paymentType != PaymentType.ERC1155_BURN) revert UnsupportedExchange();
        if (_quotePrice == 0) revert UnsupportedExchange();
        if (_tokenAddress != GDX_TOKEN) revert UnsupportedExchange();

        IERC1155Burnable(_tokenAddress).burn(_payer, _tokenId, _quotePrice);

        emit WareBought(
            _uid,
            _wareUid(_listing.ware),
            _payer,
            _listing.paymentMethod.paymentType,
            _listing.paymentMethod.tokenAddress,
            _listing.paymentMethod.tokenId,
            _purchaseUnits,
            _quotePrice,
            _context
        );
    }

    /**
     * @dev Get a quote which should return price and payment token information.
     *
     * @param _listing Structure with listing data.
     * @param _purchaseUnits Amount of ware units to be bought.
     * @return Amount in quote token.
     * @return Quote token address.
     * @return Quote token id. 
     */
    function quoteFor(address, Listing memory _listing, uint256 _purchaseUnits, bytes memory) public pure returns (uint256, address, uint256) {
        uint256 _finalQuotePrice = _listing.ware.pricePerUnit * _purchaseUnits / _listing.ware.pricePerUnitDivisor;
        return (_finalQuotePrice, _listing.paymentMethod.tokenAddress, _listing.paymentMethod.tokenId);
    }

    /**
     * @dev Get unique identifier for the ware.
     *
     * @param _ware Listing ware data.
     * @return uid for the ware.
     */
    function _wareUid(Ware memory _ware) internal pure returns (bytes32) {
        return keccak256(abi.encodePacked(_ware.wareAddress, _ware.wareId));
    }
}
