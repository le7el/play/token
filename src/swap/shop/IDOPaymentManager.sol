// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import { IERC20 } from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import { IERC721 } from "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import { SafeERC20 } from "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import { IListing } from "../../interfaces/IListing.sol";
import { IPaymentManager } from "../../interfaces/IPaymentManager.sol";

/** 
 * @title L7L token sale through seedEE vesting token with discount for L7 Investor holders.
 */
contract IDOPaymentManager is IListing, IPaymentManager {
    using SafeERC20 for IERC20;

    address public immutable SHOP;
    IERC721 public immutable L7_INVESTORS_NFT;

    // percents, the divisor would be 100
    uint256 public immutable L7_INVESTORS_DISCOUNT; 

    mapping(bytes32 => mapping(address => mapping(uint256 => bool))) public usedNFTs;

    /**
     * @dev Configure payment manager.
     *
     * @param _shopContractAddress Trusted shop contract which can authorize the payment.
     * @param _nftWithDiscount ERC721 NFT which would provide discount bonus.
     * @param _nftHolderDiscount Percents of tokens given to L7 Investor holders (should be in percents and lower than 100).
     *                           For example 93 would mean 7% discount.
     */
    constructor(address _shopContractAddress, IERC721 _nftWithDiscount, uint256 _nftHolderDiscount) {
        if (_nftHolderDiscount > 100) revert InvalidConfig();
        L7_INVESTORS_DISCOUNT = _nftHolderDiscount;
        L7_INVESTORS_NFT = _nftWithDiscount;
        SHOP = _shopContractAddress;
    }

    /**
     * @dev Process payment for sold ware, can implement discounts and other custom features.
     *
     * @param _uid Unique identifier of listing.
     * @param _listing Structure with listing data.
     * @param _payer Address which pay for the ware in listing.
     * @param _purchaseUnits Amount of ware units to be bought.
     * @param _context Additional data like specific NFT id.
     */
    function payFor(bytes32 _uid, Listing memory _listing, address _payer, uint256 _purchaseUnits, bytes memory _context) external payable {
        if (msg.sender != SHOP) revert Unauthorized();
        (uint256 _quotePrice, address _tokenAddress, uint256 _tokenId) = quoteFor(_payer, _listing, _purchaseUnits, _context);
        
        if (_listing.paymentMethod.paymentType == PaymentType.ERC20_TRANSFER) {
            if (_quotePrice == 0 || _tokenId != 0) revert UnsupportedExchange();
            IERC20(_tokenAddress).safeTransferFrom(_payer, _listing.paymentMethod.beneficiary, _quotePrice);
        } else if (_listing.paymentMethod.paymentType == PaymentType.GAS_COIN_TRANSFER) {
            if (_quotePrice > msg.value || _tokenAddress != address(0) || _tokenId != 0) revert UnsupportedExchange();
            (bool _success,) = _listing.paymentMethod.beneficiary.call{value: msg.value}("");
            if (!_success) revert PaymentFailed();
        } else {
            revert PaymentFailed();
        }

        emit WareBought(
            _uid,
            _wareUid(_listing.ware),
            _payer,
            _listing.paymentMethod.paymentType,
            _listing.paymentMethod.tokenAddress,
            _listing.paymentMethod.tokenId,
            _purchaseUnits,
            _quotePrice,
            _context
        );
    }

    /**
     * @dev Get a quote which should return price and payment token information.
     *
     * @param _listing Structure with listing data.
     * @param _purchaseUnits Amount of ware units to be bought.
     * @return Amount in quote token.
     * @return Quote token address.
     * @return Quote token id. 
     */
    function quoteFor(address _payer, Listing memory _listing, uint256 _purchaseUnits, bytes memory) public view returns (uint256, address, uint256) {
        uint256 _holderCount = L7_INVESTORS_NFT.balanceOf(_payer);
        uint256 _finalQuotePrice;
        if (_holderCount > 0) {
            _finalQuotePrice = _listing.ware.pricePerUnit * _purchaseUnits / _listing.ware.pricePerUnitDivisor * L7_INVESTORS_DISCOUNT / 100;
        } else {
            _finalQuotePrice = _listing.ware.pricePerUnit * _purchaseUnits / _listing.ware.pricePerUnitDivisor;
        }
        return (_finalQuotePrice, _listing.paymentMethod.tokenAddress, _listing.paymentMethod.tokenId);
    }

    /**
     * @dev Get unique identifier for the ware.
     *
     * @param _ware Listing ware data.
     * @return uid for the ware.
     */
    function _wareUid(Ware memory _ware) internal pure returns (bytes32) {
        return keccak256(abi.encodePacked(_ware.wareAddress, _ware.wareId));
    }
}
