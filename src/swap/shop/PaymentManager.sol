// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import { IERC20 } from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import { IERC1155 } from "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";
import { IERC721 } from "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import { SafeERC20 } from "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import { IListing } from "../../interfaces/IListing.sol";
import { IPaymentManager } from "../../interfaces/IPaymentManager.sol";
import { IPerkManager } from "../../interfaces/IPerkManager.sol";
import { IERC20Burnable } from "../../interfaces/IERC20Burnable.sol";
import { IERC721BurnableFrom } from "../../interfaces/IERC721BurnableFrom.sol";
import { IERC1155Burnable } from "../../interfaces/IERC1155Burnable.sol";

/** 
 * @title Unified interface to process payments and give individual quotes for wares bought in shop.
 * @dev Normally you deploy your own PaymentManager contract, using this one as an inspiration for different
 *      Payment types. Remove payment type checks which you don't support. Also you can implement discounts
 *      and whitelisting in such a custom contract.
 */
contract PaymentManager is IListing, IPaymentManager {
    using SafeERC20 for IERC20;

    address public immutable SHOP;
    mapping(bytes32 => mapping(address => mapping(uint256 => bool))) public usedNFTs;

    /**
     * @dev Configure payment manager.
     *
     * @param _shopContractAddress Trusted shop contract which can authorize the payment.
     */
    constructor(address _shopContractAddress) {
        SHOP = _shopContractAddress;
    }

    /**
     * @dev Process payment for sold ware, can implement discounts and other custom features.
     *
     * @param _uid Unique identifier of listing.
     * @param _listing Structure with listing data.
     * @param _payer Address which pay for the ware in listing.
     * @param _purchaseUnits Amount of ware units to be bought.
     * @param _context Additional data like specific NFT id.
     */
    function payFor(bytes32 _uid, Listing memory _listing, address _payer, uint256 _purchaseUnits, bytes memory _context) external payable {
        if (msg.sender != SHOP) revert Unauthorized();
        (uint256 _quotePrice, address _tokenAddress, uint256 _tokenId) = quoteFor(_payer, _listing, _purchaseUnits, _context);
        
        if (_listing.paymentMethod.paymentType == PaymentType.PERK_REVOKE) {
            if (_quotePrice == 0) revert UnsupportedExchange();
            uint256 _perks = IPerkManager(_tokenAddress).balanceOf(_payer, _tokenId);
            if (_perks == 0 || _perks < _quotePrice) revert PaymentFailed();
            
            IPerkManager(_tokenAddress).revokePerk(_tokenId, _payer, _quotePrice);
        } else if (_listing.paymentMethod.paymentType == PaymentType.PERK_USE) {
            if (_quotePrice != 1) revert UnsupportedExchange();
            uint256 _perks = IPerkManager(_tokenAddress).balanceOf(_payer, _tokenId);
            if (_perks == 0 || _perks < _quotePrice) revert PaymentFailed();

            (uint256 _usages,) = IPerkManager(_tokenAddress).usePerk(_tokenId, _payer, _wareUid(_listing.ware));
            if (_usages != 1) revert PaymentFailed();
        } else if (_listing.paymentMethod.paymentType == PaymentType.ERC20_TRANSFER) {
            if (_quotePrice == 0 || _tokenId != 0) revert UnsupportedExchange();
            IERC20(_tokenAddress).safeTransferFrom(_payer, _listing.paymentMethod.beneficiary, _quotePrice);
        } else if (_listing.paymentMethod.paymentType == PaymentType.ERC20_BURN) {
            if (_quotePrice == 0 || _tokenId != 0) revert UnsupportedExchange();
            IERC20Burnable(_tokenAddress).burnFrom(_payer, _quotePrice);
        // Can be easily abused if used with non-soulbound ERC20 token
        } else if (_listing.paymentMethod.paymentType == PaymentType.ERC20_HOLD) {
            if (_quotePrice == 0 || _tokenId != 0) revert UnsupportedExchange();
            if (_listing.ware.wareType != IListing.WareType.PERK_EXCLUSIVE) revert UnsupportedExchange();
            if (IERC20(_tokenAddress).balanceOf(_payer) < _quotePrice) revert PaymentFailed();
        } else if (_listing.paymentMethod.paymentType == PaymentType.ERC1155_TRANSFER) {
            if (_quotePrice == 0) revert UnsupportedExchange();
            IERC1155(_tokenAddress).safeTransferFrom(_payer, _listing.paymentMethod.beneficiary, _tokenId, _quotePrice, _context);
        } else if (_listing.paymentMethod.paymentType == PaymentType.ERC1155_BURN) {
            if (_quotePrice == 0) revert UnsupportedExchange();
            IERC1155Burnable(_tokenAddress).burn(_payer, _tokenId, _quotePrice);
        // Can be easily abused if used with non-soulbound ERC1155 token
        } else if (_listing.paymentMethod.paymentType == PaymentType.ERC1155_HOLD) {
            if (_quotePrice == 0) revert UnsupportedExchange();
            if (_listing.ware.wareType != IListing.WareType.PERK_EXCLUSIVE) revert UnsupportedExchange();
            if (IERC1155(_tokenAddress).balanceOf(_payer, _tokenId) < _quotePrice) revert PaymentFailed();
        } else if (_listing.paymentMethod.paymentType == PaymentType.ERC721_BURN) {
            if (_quotePrice != 1) revert UnsupportedExchange();
            IERC721BurnableFrom(_tokenAddress).burnFrom(_payer, _tokenId);
        } else if (_listing.paymentMethod.paymentType == PaymentType.GAS_COIN_TRANSFER) {
            if (_quotePrice > msg.value || _tokenAddress != address(0) || _tokenId != 0) revert UnsupportedExchange();
            (bool _success,) = _listing.paymentMethod.beneficiary.call{value: msg.value}("");
            if (!_success) revert PaymentFailed();
        } else if (_listing.paymentMethod.paymentType == PaymentType.ERC721_HOLD || _listing.paymentMethod.paymentType == PaymentType.ERC721_TRANSFER) {
            if (_quotePrice != 1) revert UnsupportedExchange();
            // Specific NFT can be enfroced
            uint256 _nftId = _tokenId;
            // Or any NFT from a given collection
            if (_nftId == 0) (_nftId) = abi.decode(_context, (uint256));
            // Fail if specific NFT can't be found at all
            if (_nftId == 0) revert UnsupportedExchange();

            if (_listing.paymentMethod.paymentType == PaymentType.ERC721_TRANSFER) {
                IERC721(_listing.paymentMethod.tokenAddress).safeTransferFrom(_payer, _listing.paymentMethod.beneficiary, _nftId);
            } else if (_listing.paymentMethod.paymentType == PaymentType.ERC721_HOLD) {
                if (IERC721(_listing.paymentMethod.tokenAddress).ownerOf(_nftId) != _payer) revert PaymentFailed();
                if (usedNFTs[_uid][_listing.paymentMethod.tokenAddress][_nftId]) revert PaymentFailed();
                usedNFTs[_uid][_listing.paymentMethod.tokenAddress][_nftId] = true;
            // Fail in case of misconfiguration
            } else {
                revert UnsupportedExchange();
            }
        } else {
            revert PaymentFailed();
        }

        emit WareBought(
            _uid,
            _wareUid(_listing.ware),
            _payer,
            _listing.paymentMethod.paymentType,
            _listing.paymentMethod.tokenAddress,
            _listing.paymentMethod.tokenId,
            _purchaseUnits,
            _quotePrice,
            _context
        );
    }

    /**
     * @dev Get a quote which should return price and payment token information.
     *
     * @param _listing Structure with listing data.
     * @param _purchaseUnits Amount of ware units to be bought.
     * @return Amount in quote token.
     * @return Quote token address.
     * @return Quote token id. 
     */
    function quoteFor(address, Listing memory _listing, uint256 _purchaseUnits, bytes memory) public pure returns (uint256, address, uint256) {
        uint256 _finalQuotePrice = _listing.ware.pricePerUnit * _purchaseUnits / _listing.ware.pricePerUnitDivisor;
        return (_finalQuotePrice, _listing.paymentMethod.tokenAddress, _listing.paymentMethod.tokenId);
    }

    /**
     * @dev Get unique identifier for the ware.
     *
     * @param _ware Listing ware data.
     * @return uid for the ware.
     */
    function _wareUid(Ware memory _ware) internal pure returns (bytes32) {
        return keccak256(abi.encodePacked(_ware.wareAddress, _ware.wareId));
    }
}
