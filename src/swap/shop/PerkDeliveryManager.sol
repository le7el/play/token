// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import { DeliveryManager } from "./DeliveryManager.sol";
import { IPerkManager } from "../../interfaces/IPerkManager.sol";

/** 
 * @title Delivery of perk bought in shop.
 *
 * @dev Each shop seller should deploy their own delivery manager, as there is always a risk of malicious
 *      wareAddress in Listing which would be able to drain this contract. For the same reason shared
 *      DeliveryManager contracts are not natively supported! 
 */
contract PerkDeliveryManager is DeliveryManager {
    /**
     * @dev Configure delivery manager.
     *
     * @param _owner Address which can manage ware availability for delivery.
     * @param _shopContractAddress Trusted shop contract which can authorize the delivery.
     */
    constructor(address _owner, address _shopContractAddress) DeliveryManager(_owner, _shopContractAddress) {}

    /**
     * @dev Delivers a bought ware, it's expected that payments and all the validations were handled earlier on a SHOP contract level.   
     *
     * @param _uid Unique identifier of listing.
     * @param _listing Structure with listing data.
     * @param _buyer Address which would get the delivery.
     * @param _purchaseUnits Amount of ware units to be delivered.
     * @param _context Additional data like specific NFT id.
     */
    function deliver(
        bytes32 _uid,
        Listing memory _listing,
        address _buyer,
        uint256 _purchaseUnits,
        bytes memory _context
    ) public override isValidDeliveryParams(_uid, _listing, _purchaseUnits) {
        if (_listing.ware.wareType != WareType.PERK && _listing.ware.wareType != WareType.PERK_EXCLUSIVE) revert UnsupportedWareType();
        IPerkManager(_listing.ware.wareAddress).grantPerk(_listing.ware.wareId, _buyer, _purchaseUnits);
        super.deliver(_uid, _listing, _buyer, _purchaseUnits, _context);
    }
}