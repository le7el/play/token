// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import { IPerkManager } from "../../interfaces/IPerkManager.sol";
import { ReentrancyGuard } from "@openzeppelin/contracts/utils/ReentrancyGuard.sol";
import { IERC1155 } from "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";

/** 
 * @title Database to store on-chain perks which can't be represented as NFTs or tokens.
 *        It integrates ERC1155 interface to simplify wallet's and 3rd party integrations.
 */
contract PerkManager is IPerkManager, IERC1155, ReentrancyGuard {
    uint256 public nextPerkId = 1;
    mapping(uint256 => Perk) public perks;
    mapping(uint256 => mapping(address => uint256)) public ledger;
    mapping(uint256 => mapping(address => bool)) public perkManagers;
    mapping(uint256 => mapping(address => mapping(bytes32 => uint256))) public perkUsages;
    mapping(uint256 => mapping(address => uint256)) public perkTotalUsages;

    struct Perk{
        address owner;
        uint256 totalSupply;
        uint256 circulatingSupply;
        bool exclusive;
        bytes metadata;
    }

    event PerkRegistered(
        uint256 indexed id,
        address indexed owner,
        uint256 totalSupply,
        bool exclusive,
        bytes metadata
    );

    event PerkUpdated(
        uint256 indexed id,
        address indexed oldOwner,
        address indexed newOwner,
        uint256 totalSupply,
        bool exclusive,
        bytes metadata
    );

    event PerkManagerAdded(
        uint256 indexed id,
        address indexed manager
    );

    event PerkManagerRevoked(
        uint256 indexed id,
        address indexed manager
    );

    event PerkGranted(
        uint256 indexed id,
        address indexed user,
        uint256 amount
    );

    event PerkRevoked(
        uint256 indexed id,
        address indexed user,
        uint256 amount
    );

    event PerkUsed(
        uint256 indexed id,
        address indexed user,
        bytes32 indexed usageUid,
        uint256 usages,
        uint256 totalUsages
    );

    error Unauthorized();
    error InvalidPerkParams();
    error OutOfStock();
    error OnePerWallet();
    error NotEnoughPerks();
    error NotImplemented();

    // Used to suppress warnings about function state mutability and unreachable code warnings
    // in placeholder functions which are not implemented.
    modifier notImplemented() {
        if (true) revert NotImplemented();
        _;
    }

    /**
     * @dev Setup perk manager contract.
     */
    constructor() {}

    /**
     * @dev Return perk's metadata.
     *
     * @param _id Unique id of perk.
     */
    function uri(uint256 _id) public view returns (string memory) {
        return string(perks[_id].metadata);
    }

    /**
     * @dev Amount of a specific perk owned by the user.
     *
     * @param _user Address which owns the perk.
     * @param _id Unique id of perk.
     */
    function balanceOf(address _user, uint256 _id) public override(IPerkManager, IERC1155) view returns (uint256) {
        return ledger[_id][_user];
    }

    /**
     * @dev Get batched perk balances for each user.
     *
     * @param _users Addresses which own perks.
     * @param _ids Unique ids of perks.
     * @return array with balances.
     */
    function balanceOfBatch(
        address[] calldata _users,
        uint256[] calldata _ids
    ) public override(IERC1155, IPerkManager) view returns (uint256[] memory) {
        if (_users.length != _ids.length) revert InvalidPerkParams();
        uint256[] memory _balances = new uint256[](_users.length);
        uint256 _i = 0;
        for (_i = 0; _i < _users.length; _i++) {
            _balances[_i] = ledger[_ids[_i]][_users[_i]];
        }
        return _balances;
    }

    /**
     * @dev Not implemented, perk transfer is not possible.
     */
    // solhint-disable-next-line no-empty-blocks
    function setApprovalForAll(address, bool) external override notImplemented {}

    /**
     * @dev Not implemented, perk transfer is not possible.
     */
    function isApprovedForAll(address, address) public override pure returns (bool) {
        return false;
    }

    /**
     * @dev Not implemented, perk transfer is not possible.
     */
    // solhint-disable-next-line func-mutability
    function safeTransferFrom(address, address, uint256, uint256, bytes calldata) external override notImplemented {}

    /**
     * @dev Not implemented, perk transfer is not possible.
     */
    // solhint-disable-next-line func-mutability
    function safeBatchTransferFrom(address, address, uint256[] calldata, uint256[] calldata, bytes calldata) external override notImplemented {}

    /**
     * @dev List new on-chain perk.
     *
     * @param _owner Address which can change perk's settings.
     * @param _totalSupply Max amount of perks to be distributed.
     * @param _exclusive If true, only one perk can be granted to a user.
     * @param _metadata Perk's metadata.
     * @return registered perk id.
     */
    function registerPerk(address _owner, uint256 _totalSupply, bool _exclusive, bytes memory _metadata) external nonReentrant returns (uint256) {
        uint256 _id = nextPerkId;
        nextPerkId = _id + 1;

        if (_owner == address(0)) revert InvalidPerkParams();
        if (perks[_id].owner != address(0)) revert InvalidPerkParams();
        if (_totalSupply == 0) revert InvalidPerkParams();

        perks[_id] = Perk(_owner, _totalSupply, 0, _exclusive, _metadata);
        emit PerkRegistered(_id, _owner, _totalSupply, _exclusive, _metadata);
        return _id;
    }

    /**
     * @dev Update existing on-chain perk.
     *      Exclusivity can't be turned on because some users can already have 2 or more perks.
     *
     * @param _id Perk id to update.
     * @param _newOwner New owner to administer the perk, pass address(0) for now change.
     * @param _totalSupply Max amount of perks to be distributed.
     * @param _exclusiveOff Can make exlisive perk non exclusive.
     * @param _metadata Perk's metadata.
     */
    function updatePerk(uint256 _id, address _newOwner, uint256 _totalSupply, bool _exclusiveOff, bytes memory _metadata) external {
        Perk memory _perk = perks[_id];
        if (_perk.owner != msg.sender) revert Unauthorized();
        if (_totalSupply == 0) revert InvalidPerkParams();

        if (_newOwner != address(0) && _perk.owner != _newOwner) perks[_id].owner = _newOwner;
        if (_perk.totalSupply != _totalSupply) {
            if (_totalSupply < _perk.circulatingSupply) revert InvalidPerkParams();
            perks[_id].totalSupply = _totalSupply;
        }

        bool _exclusivity = _perk.exclusive;
        if (_perk.exclusive && _exclusiveOff) {
            _exclusivity = false;
            perks[_id].exclusive = false;
        }

        bytes32 _metadataUid = keccak256(_metadata);
        if (keccak256(_perk.metadata) != _metadataUid && _metadataUid != keccak256("\x00")) perks[_id].metadata = _metadata;
    
        emit PerkUpdated(_id, _perk.owner, _newOwner, _totalSupply, _exclusivity, _metadata);
    }

    /**
     * @dev Perk owner can allow manager address to grant and revoke the perk.
     *
     * @param _id Id of perk.
     * @param _manager Address which grant and revoke perk.
     * @param _status true to grant rights, false to revoke them.
     */
    function togglePerkManager(uint256 _id, address _manager, bool _status) external {
        if (perks[_id].owner != msg.sender) revert Unauthorized();
        perkManagers[_id][_manager] = _status;
        if (_status) {
            emit PerkManagerAdded(_id, _manager);
        } else {
            emit PerkManagerRevoked(_id, _manager);
        }
    }

    /**
     * @dev Give on-chain perk to the user.
     *
     * @param _id Unique id of perk.
     * @param _user Address which should get the perk.
     * @param _amount Amount of perks to be granted.
     */
    function grantPerk(uint256 _id, address _user, uint256 _amount) external {
        if (_amount == 0) revert InvalidPerkParams();
        if (_user == address(0)) revert InvalidPerkParams();
        uint256 _circulatingSupply = perks[_id].circulatingSupply;
        if (perks[_id].totalSupply < _circulatingSupply + _amount) revert OutOfStock();
        if (perks[_id].exclusive && (_amount != 1 || balanceOf(_user, _id) > 0)) revert OnePerWallet();
        if (perks[_id].owner != msg.sender && !perkManagers[_id][msg.sender]) revert Unauthorized();

        perks[_id].circulatingSupply = _circulatingSupply + _amount;
        ledger[_id][_user] += _amount;
        emit PerkGranted(_id, _user, _amount);
        emit TransferSingle(msg.sender, address(0), _user, _id, _amount);
    }

    /**
     * @dev Revoke on-chain perk from the user. 
     *      Owner of the perk, or it's manager can do the revokation.
     *
     * @param _id Id of perk.
     * @param _user Address from whick perk is revoked.
     * @param _amount Amount of perks to be revoked.
     */
    function revokePerk(uint256 _id, address _user, uint256 _amount) external nonReentrant {
        if (_amount == 0) revert InvalidPerkParams();
        if (balanceOf(_user, _id) < _amount) revert NotEnoughPerks();
        if (msg.sender != _user && !perkManagers[_id][msg.sender] && perks[_id].owner != msg.sender) revert Unauthorized();

        perks[_id].circulatingSupply -= _amount;
        ledger[_id][_user] -= _amount;
        emit PerkRevoked(_id, _user, _amount);
        emit TransferSingle(msg.sender, _user, address(0), _id, _amount);
    }

    /**
     * @dev Register usage of perk in scope of some usage id.
     *      Usually it's used for redemption of extra rewards.
     *
     * @param _id Id of perk.
     * @param _user Address from whick perk is used.
     * @param _usageUid Unique identifier to prevent double usage.
     * @return total count of usages.
     */
    function usePerk(uint256 _id, address _user, bytes32 _usageUid) external nonReentrant returns (uint256, uint256) {
        if (balanceOf(_user, _id) == 0) revert NotEnoughPerks();
        if (msg.sender != _user && !perkManagers[_id][msg.sender] && perks[_id].owner != msg.sender) revert Unauthorized();
        uint256 _usages = perkUsages[_id][_user][_usageUid] + 1;
        uint256 _totalUsages = perkTotalUsages[_id][_user] + 1;
        perkUsages[_id][_user][_usageUid] = _usages;
        perkTotalUsages[_id][_user] = _totalUsages;
        emit PerkUsed(_id, _user, _usageUid, _usages, _totalUsages);
        return (_usages, _totalUsages);
    }

    /**
     * @dev Checks if contract supports specific interface.
     *
     * @param _interfaceId Id of interface to check.
     */
    function supportsInterface(bytes4 _interfaceId) external override pure returns (bool) {
        return
            _interfaceId == type(IERC1155).interfaceId ||
            _interfaceId == type(IPerkManager).interfaceId;
    }
}