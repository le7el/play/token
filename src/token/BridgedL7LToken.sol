// SPDX-License-Identifier: MPL-2.0

pragma solidity ^0.8.20;

import { Ownable } from "@openzeppelin/contracts/access/Ownable.sol";
import { ERC20Permit } from "@openzeppelin/contracts/token/ERC20/extensions/ERC20Permit.sol";
import { ERC20Burnable } from "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import { OFT } from "@layerzerolabs/lz-evm-oapp-v2/contracts/oft/OFT.sol";

/** 
 * @title Bridged version of L7L token with minting fully managed by OFT.
 */
contract BridgedL7LToken is OFT, ERC20Burnable, ERC20Permit {
    /** 
     * @dev Configure bridged version of L7L token.
     * 
     * @param _layerZeroEndpoint local network endpoint address ref. https://docs.layerzero.network/v2/developers/evm/technical-reference/endpoints
     * @param _owner token owner used as a delegate in LayerZero Endpoint
     */
    constructor(address _layerZeroEndpoint, address _owner)
        OFT("LE7EL", "L7L", _layerZeroEndpoint, _owner)
        ERC20Permit("LE7EL")
        Ownable(_owner) {}
}