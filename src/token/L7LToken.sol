// SPDX-License-Identifier: MPL-2.0

pragma solidity ^0.8.20;

import { Ownable } from "@openzeppelin/contracts/access/Ownable.sol";
import { ERC20Permit } from "@openzeppelin/contracts/token/ERC20/extensions/ERC20Permit.sol";
import { ERC20Burnable } from "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import { OFT } from "@layerzerolabs/lz-evm-oapp-v2/contracts/oft/OFT.sol";
import { IMintingRights } from "../interfaces/IMintingRights.sol";

/** 
 * @title L7L token is the main fungible token in LE7EL ecosystem.
 */
contract L7LToken is OFT, ERC20Burnable, ERC20Permit, IMintingRights {
    // No more than MAX_TOTAL_SUPPLY can be minted.
    uint256 constant public MAX_TOTAL_SUPPLY = 1000000000 ether;
    uint256 constant public CANONIC_CHAIN = 1; // Ethereum

    address public minter;
    address public pendingMinter;
    address public staticMinter;
    uint256 public mintedCanonically = 0;

    event MintingRightsTransferStarted(address indexed from, address to);
    event MintingRightsTransferFinished(address indexed newMinter);

    error ReachedMaxSupply();
    error Unauthorized();
    error OnlyMintOnCanonicChain();
    
    /** 
     * @dev Configure L7L token.
     * 
     * @param _layerZeroEndpoint local network endpoint address ref. https://docs.layerzero.network/v2/developers/evm/technical-reference/endpoints
     * @param _owner token owner used as a delegate in LayerZero Endpoint
     */
    constructor(address _layerZeroEndpoint, address _owner)
        OFT("LE7EL", "L7L", _layerZeroEndpoint, _owner)
        ERC20Permit("LE7EL")
        Ownable(_owner) {
            minter = msg.sender;
        }

    /** 
     * @dev Static minter can't be changed after it's set.
     *
     * @param _staticMinter new static minter contract.
     */
    function assignStaticMinter(address _staticMinter) external onlyOwner {
        if (staticMinter != address(0) || _staticMinter == address(0)) revert Unauthorized();
        staticMinter = _staticMinter;
    }

    /** 
     * @dev Minter can be changed to a minter contract.
     *
     * @param _newMinter new minter contract
     */
    function transferMinter(address _newMinter) external {
        address _minter = minter;
        if (msg.sender != _minter) revert Unauthorized();
        pendingMinter = _newMinter;
        emit MintingRightsTransferStarted(_minter, _newMinter);
    }

    /** 
     * @dev Accept minting rights for the token.
     */
    function acceptMinter() external {
        address _pendingMinter = pendingMinter;
        if (msg.sender != _pendingMinter) revert Unauthorized();
        minter = _pendingMinter;
        emit MintingRightsTransferFinished(_pendingMinter);
    }

    /** 
     * @dev Owner can mint new tokens until the supply of 1b L7L is reached.
     *
     * @param _to address where new tokens are minted.
     * @param _amount amount of tokens to be minted.
     */
    function mint(address _to, uint256 _amount) public {
        if (block.chainid != CANONIC_CHAIN) revert OnlyMintOnCanonicChain();
        if (msg.sender != minter && msg.sender != staticMinter) revert Unauthorized();
        uint256 _mintedCanonically = mintedCanonically;
        uint256 _newCanonicSupply = _mintedCanonically + _amount;
        if (_newCanonicSupply > MAX_TOTAL_SUPPLY) revert ReachedMaxSupply();
        mintedCanonically = _newCanonicSupply;
        _mint(_to, _amount);
    }
}