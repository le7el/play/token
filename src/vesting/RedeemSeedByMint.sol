// SPDX-License-Identifier: MPL-2.0

pragma solidity ^0.8.20;

import { IERC20 } from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import { SafeERC20 } from "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import { Pausable } from "@openzeppelin/contracts/utils/Pausable.sol";
import { ReentrancyGuard } from "@openzeppelin/contracts/utils/ReentrancyGuard.sol";
import { Ownable } from "@openzeppelin/contracts/access/Ownable.sol";
import { Ownable2Step } from "@openzeppelin/contracts/access/Ownable2Step.sol";
import { IMinter } from "../interfaces/IMinter.sol";

/** 
 * @title Redeem EESeed tokens to L7L tokens.
 *
 * @dev The total supply of seedEE tokens is 116,635,927 exchange rate is 1:1, no new seedEE is allowed to mint,
 * so a total of 116,635,927 $L7L would be minted by this contract.
 *
 * whitelisting code and EIP-712 is stolen from Gnosis IDO contracts: 
 * https://github.com/gnosis/ido-contracts/blob/8427e9f2de730fab837db67bc3b00abddb84b0b3/contracts/allowListExamples/AllowListOffChainManaged.sol
 */
contract RedeemSeedByMint is Ownable2Step, Pausable, ReentrancyGuard {
    using SafeERC20 for IERC20;

    IERC20 public immutable SEED_TOKEN_ADDRESS;
    IERC20 public immutable FINAL_MINTER_ADDRESS;

    // 1 for 1 to 1 redemption, if it's 10, then final tokens will be 10 times more than seed tokens burned.
    uint256 public immutable EXCHANGE_RATE;

    // 1 for 1 to 1 redemption, if it's 10, then final tokens will be 10 times less than seed tokens burned.
    uint256 public immutable EXCHANGE_RATE_BASE;

    // 116,635,927 $L7L would be minted by this contract.
    uint256 public immutable MINT_CAP;

    // UNIX timestamp when vesting is started.
    uint256 public immutable VESTING_STARTS;

    // UNIX timestamp when vesting is ended.
    uint256 public immutable VESTING_ENDS;

    // Total tokens minted by this contract.
    uint256 public minted;

    // Total tokens which are being vested for the address
    mapping(address => uint256) public vestingAmount;

    // Tokens which are were claimed by the address.
    mapping(address => uint256) public claimedAmount;

    // Compomised wallets whose claims should go to replacement wallets.
    mapping(address => address) public compromisedWallets;

    event Claimed(address indexed owner, address indexed replacement, uint256 seedAmount, uint256 finalAmount);
    event CompromisedWallet(address indexed compromisedWallet, address indexed replacementWallet, bytes signature);

    error InvalidVestingTime();
    error NoClaim();
    error NothingToClaim();
    error Unauthorized();
    error ReachedMintCap();

    /**
     * @dev Transfer ownership to DAO address and setup tokens.
     *
     * @param _owner DAO address of redemption contract.
     * @param _seedTokenAddress Seed token address which should be burned.
     * @param _finalMinterAddress Final minter of token which should be owned.
     * @param _exchangeRate How many final tokens are given for seed token.
     * @param _exchangeRateBase Base is used when the amount of final tokens are less than seed tokens.
     * @param _vestingStarts UNIX timestamp when vesting is started.
     * @param _vestingEnds UNIX timestamp when vesting is ended.
     */
    constructor(
        address _owner,
        IERC20 _seedTokenAddress,
        IERC20 _finalMinterAddress,
        uint256 _exchangeRate,
        uint256 _exchangeRateBase,
        uint256 _vestingStarts,
        uint256 _vestingEnds,
        uint256 _mintCap
    ) Ownable(_owner) {
        if (_vestingStarts < block.timestamp) revert InvalidVestingTime();
        if (_vestingStarts > _vestingEnds) revert InvalidVestingTime();
        SEED_TOKEN_ADDRESS = _seedTokenAddress;
        FINAL_MINTER_ADDRESS = _finalMinterAddress;
        EXCHANGE_RATE = _exchangeRate;
        EXCHANGE_RATE_BASE = _exchangeRateBase;
        VESTING_STARTS = _vestingStarts;
        VESTING_ENDS = _vestingEnds;
        MINT_CAP = _mintCap;
    }

    /**
     * @dev Claim vested amount, minting the new tokens.
     * It's implied that no seed tokens would be minted after the release of final token.
     * Seed tokens are soulbonded.
     */
    function claim() external {
        claim(msg.sender);
    }

    /**
     * @dev Claim vested amount, minting the new tokens.
     *
     * @param _owner Address for which the claim is done.
     */
    function claim(address _owner) public nonReentrant whenNotPaused {
        uint256 _vestingAmount = vestingAmount[_owner];
        uint256 _currentSeedBalance = SEED_TOKEN_ADDRESS.balanceOf(_owner);
        if (_vestingAmount == 0 && _currentSeedBalance > 0) {
            _vestingAmount = _currentSeedBalance;
            vestingAmount[_owner] = _currentSeedBalance;
        }
        if (_vestingAmount <= 0) revert NoClaim();

        uint256 _claimed = claimedAmount[_owner];
        uint256 _claimable = _vestedAmountToClaim(_vestingAmount, _claimed);
        if (_claimable <= 0) revert NothingToClaim();

        claimedAmount[_owner] = _claimed + _claimable;
        uint256 _finalTokenAmount = _claimable * EXCHANGE_RATE / EXCHANGE_RATE_BASE;
        address _replacement = compromisedWallets[_owner];
        if (_replacement == address(0)) _replacement = _owner;
        if (msg.sender != _replacement) revert Unauthorized();
        uint256 _newMinted = minted + _finalTokenAmount;
        if (_newMinted > MINT_CAP) revert ReachedMintCap();
        minted = _newMinted;
        IMinter(address(FINAL_MINTER_ADDRESS)).mint(_replacement, _finalTokenAmount);
        emit Claimed(_owner, _replacement, _claimable, _finalTokenAmount);
    }

    /**
     * @dev Total amount of tokens which were vested.
     *
     * @param _user Wallet for lookup.
     * @return seed tokens available for redemtion.
     */
    function vestedAmountToClaim(address _user) public view returns (uint256) {
        uint256 _vestingAmount = vestingAmount[_user];
        if (_vestingAmount == 0) {
            _vestingAmount = SEED_TOKEN_ADDRESS.balanceOf(_user);
        }
        return _vestedAmountToClaim(_vestingAmount, claimedAmount[_user]);
    }

    /**
     * @dev Redirect all claims from one address to another.
     */
    function adminCompromisedWallet(address _compromisedWallet, address _replacementWallet, bytes memory _signature) external onlyOwner() {
        compromisedWallets[_compromisedWallet] = _replacementWallet;
        emit CompromisedWallet(_compromisedWallet, _replacementWallet, _signature);
    }

    /**
     * @dev Pauses all redemptions.
     */
    function adminPause() external onlyOwner() {
        _pause();
    }

    /**
     * @dev Unpauses all redemptions.
     */
    function adminUnpause() external onlyOwner {
        _unpause();
    }

    /**
     * @dev Gas optimised view to get vested amount.
     *
     * @param _vestingAmount Total amount of tokens which are vested.
     * @param _claimedAmount Amount of seed tokens which were claimed already.
     * @return seed tokens available for redemtion.
     */
    function _vestedAmountToClaim(uint256 _vestingAmount, uint256 _claimedAmount) private view returns (uint256) {
        uint256 _releaseTime = VESTING_ENDS;
        if (_releaseTime <= block.timestamp) {
            return _vestingAmount - _claimedAmount;
        } else {
            uint256 _startTime = VESTING_STARTS;
            if (block.timestamp < _startTime) return 0;
            uint256 _totalVestSecs = _releaseTime - _startTime;
            uint256 _vestedSecs = block.timestamp - _startTime;
            return _vestingAmount * _vestedSecs / _totalVestSecs - _claimedAmount;
        }
    }
}
