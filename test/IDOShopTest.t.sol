// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import {IERC20Errors} from "@openzeppelin/contracts/interfaces/draft-IERC6093.sol";
import {IERC721} from "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import {Pausable} from "@openzeppelin/contracts/utils/Pausable.sol";
import {IAccessControl} from "@openzeppelin/contracts/access/IAccessControl.sol";
import {TestL7LWithDeployments} from "./util/TestL7LWithDeployments.sol";
import {IListing} from "src/interfaces/IListing.sol";
import {IPaymentManager} from "src/interfaces/IPaymentManager.sol";
import {IDeliveryManager} from "src/interfaces/IDeliveryManager.sol";
import {Shop} from "src/swap/Shop.sol";
import {IDOPaymentManager} from "src/swap/shop/IDOPaymentManager.sol";
import {EESeedDeliveryManager} from "src/swap/shop/EESeedDeliveryManager.sol";
import {EESeedToken} from "src/token/EESeedToken.sol";
import {PaymentTokenMock} from "src/mocks/PaymentTokenMock.sol";
import {MockERC721} from "solmate/test/utils/mocks/MockERC721.sol";

contract IDOShopTest is TestL7LWithDeployments {
    address internal admin = address(1);
    address internal seller1 = address(2);
    address internal seller2 = address(6);
    address internal buyer1 = address(3);
    address internal buyer2 = address(4);
    address internal buyer3 = address(5);
    address internal beneficiary = address(7);

    Shop internal _shop;
    IDOPaymentManager internal _paymentManager;
    EESeedDeliveryManager internal _deliveryManager;
    EESeedToken internal _seedToken;
    PaymentTokenMock internal _usdcToken;
    MockERC721 internal _l7Nft;


    bytes32 internal _listingGasCoin = keccak256("L7L_IDO_ETH_LISTING");
    bytes32 internal _listingUSDC = keccak256("L7L_IDO_USDC_LISTING");

    function setUp() public {
        vm.label(admin, "Admin");
        vm.label(seller1, "Seller 1");
        vm.label(buyer1, "Buyer 1");
        vm.label(buyer2, "Buyer 2");
        vm.chainId(1);

        // Setup smart contracts
        vm.startPrank(admin);
        _shop = new Shop(admin, admin);
        _l7Nft = new MockERC721("L7 Investors", "L7I");
        _paymentManager = new IDOPaymentManager(address(_shop), IERC721(address(_l7Nft)), 93);
        _deliveryManager = new EESeedDeliveryManager(admin, address(_shop));
        _seedToken = new EESeedToken(admin);
        _deliveryManager.adminToggleWareAddressAuthorization(address(_seedToken), true);
        _shop.grantRole(_shop.SELLER_ROLE(), seller1);
        _shop.grantRole(_shop.SELLER_ROLE(), seller2);
        _seedToken.grantRole(_seedToken.MINTER_ROLE(), address(_deliveryManager));
        _usdcToken = new PaymentTokenMock(buyer1);
        vm.stopPrank();
    }

    function testIDOShopGasCoinPurchase() public {
        _gasCoinListing();
        
        vm.startPrank(buyer1);
        assertTrue(_seedToken.balanceOf(buyer1) == 0);
        vm.deal(buyer1, 2 ether);
        _shop.exchange{value: 1 ether}(_listingGasCoin, 1 ether, 58888 ether, "\x00");
        assertTrue(_seedToken.balanceOf(buyer1) == 58888 ether);
        assertTrue(beneficiary.balance == 1 ether);
        assertTrue(buyer1.balance == 1 ether);
        vm.stopPrank();
    }

    function testIDOShopUSDCPurchase() public {
        _usdcListing();
        
        vm.startPrank(buyer1);
        assertTrue(_seedToken.balanceOf(buyer1) == 0);
        uint256 _usdcDecimalDevisor = 10 ** 6;
        _usdcToken.approve(address(_paymentManager), 1000 * _usdcDecimalDevisor);
        _shop.exchange(_listingUSDC, 1000 * _usdcDecimalDevisor, 22222222222223000000000, "\x00");
        assertTrue(_seedToken.balanceOf(buyer1) == 22222222222223000000000);
        assertTrue(_usdcToken.balanceOf(buyer1) == 10000000000000 - 1000 * _usdcDecimalDevisor);
        assertTrue(_usdcToken.balanceOf(beneficiary) == 1000 * _usdcDecimalDevisor);
        vm.stopPrank();
    }

    function testDiscountGasCoin() public {
        _gasCoinListing();
        
        vm.startPrank(buyer1);
        _l7Nft.mint(buyer1, 10);
        assertTrue(_seedToken.balanceOf(buyer1) == 0);
        vm.deal(buyer1, 2 ether);
        _shop.exchange{value: 999999999999575464}(_listingGasCoin, 999999999999575464, 63320430107500000000000, "\x00");
        assertTrue(_seedToken.balanceOf(buyer1) == 63320430107500000000000);
        assertTrue(beneficiary.balance == 999999999999575464);
        assertTrue(buyer1.balance == 2 ether - 999999999999575464);
        vm.stopPrank();
    }

    function testDiscountUSDC() public {
        _usdcListing();
        
        vm.startPrank(buyer1);
        _l7Nft.mint(buyer1, 10);
        assertTrue(_seedToken.balanceOf(buyer1) == 0);
        _usdcToken.approve(address(_paymentManager), 999999999);
        _shop.exchange(_listingUSDC, 999999999, 23894862604500000000000, "\x00");
        assertTrue(_seedToken.balanceOf(buyer1) == 23894862604500000000000);
        assertTrue(_usdcToken.balanceOf(buyer1) == 10000000000000 - 999999999);
        assertTrue(_usdcToken.balanceOf(beneficiary) == 999999999);
        vm.stopPrank();
    }

    function testNoMoreThan100mTokensSold() public {
        _usdcListing();
        
        vm.startPrank(buyer1);
        assertTrue(_seedToken.balanceOf(buyer1) == 0);
        uint256 _usdcDecimalDevisor = 10 ** 6;
        _usdcToken.approve(address(_paymentManager), 4499999999999);
        _shop.exchange(_listingUSDC, 4499999999999, 100000000 ether, "\x00");
        assertTrue(_seedToken.balanceOf(buyer1) == 100000000 ether);
        assertTrue(_usdcToken.balanceOf(buyer1) == 10000000000000 - 4499999999999);
        assertTrue(_usdcToken.balanceOf(beneficiary) == 4499999999999);
        _usdcToken.approve(address(_paymentManager), 1000 * _usdcDecimalDevisor);
        vm.expectRevert(Shop.OutOfStock.selector);
        _shop.exchange(_listingUSDC, 1000 * _usdcDecimalDevisor, 22222222222223000000000, "\x00");
        vm.stopPrank();
    }

    function testValidListing1() public {
        vm.startPrank(seller1);
        _shop.list(
            keccak256("TEST1"),
            IListing.PaymentMethod(address(0), 0, IListing.PaymentType.ERC1155_TRANSFER, beneficiary, address(_paymentManager)),
            IListing.Ware(address(_seedToken), 0, IListing.WareType.ERC20_MINT, 1, 1, 1, address(_deliveryManager))
        );
        vm.stopPrank();
        
        vm.startPrank(buyer1);
        vm.expectRevert(IPaymentManager.PaymentFailed.selector);
        _shop.exchange(keccak256("TEST1"), 1, 1, "\x00");
        vm.stopPrank();
    }

    function testValidListing2() public {
        vm.startPrank(seller1);
        _shop.list(
            keccak256("TEST1"),
            IListing.PaymentMethod(address(0), 0, IListing.PaymentType.GAS_COIN_TRANSFER, beneficiary, address(_paymentManager)),
            IListing.Ware(address(_seedToken), 0, IListing.WareType.ERC20_TRANSFER, 1, 1, 1, address(_deliveryManager))
        );
        vm.stopPrank();

        vm.prank(admin);
        _deliveryManager.adminToggleListingAuthorization(keccak256("TEST1"), true);
        
        vm.startPrank(buyer1);
        vm.deal(buyer1, 1 ether);
        vm.expectRevert(IDeliveryManager.UnsupportedWareType.selector);
        _shop.exchange{value: 1}(keccak256("TEST1"), 1, 1, "\x00");
        vm.stopPrank();
    }

    function testGettingQuotes() public {
        _usdcListing();

        (uint256 _quote,,) = _shop.quoteFor(_listingUSDC, msg.sender, 1 ether, "\x00");
        assertTrue(_quote == 44999);
        (uint256 _offer1,,) = _shop.offerOf(_listingUSDC, msg.sender, 1000000, "\x00");
        assertTrue(_offer1 == 22.222222222223 ether);
        (uint256 _offer2,,) = _shop.offerOf(_listingUSDC, msg.sender, 44999, "\x00");
        assertTrue(_offer2 == 999977777777812777); // ~1 ether
    }


    /////////////
    // HELPERS //
    /////////////

    function _gasCoinListing() private {
        vm.startPrank(seller1);
        _shop.list(
            _listingGasCoin,
            IListing.PaymentMethod(address(0), 0, IListing.PaymentType.GAS_COIN_TRANSFER, beneficiary, address(_paymentManager)),
            IListing.Ware(address(_seedToken), 0, IListing.WareType.ERC20_MINT, 100000000 ether, 1, 58888, address(_deliveryManager))
        );
        vm.stopPrank();

        vm.prank(admin);
        _deliveryManager.adminToggleListingAuthorization(_listingGasCoin, true);
    }

    function _usdcListing() private {
        vm.startPrank(seller1);
        _shop.list(
            _listingUSDC,
            IListing.PaymentMethod(address(_usdcToken), 0, IListing.PaymentType.ERC20_TRANSFER, beneficiary, address(_paymentManager)),
            IListing.Ware(address(_seedToken), 0, IListing.WareType.ERC20_MINT, 100000000 ether, 1, 22222222222223, address(_deliveryManager))
        );
        vm.stopPrank();

        vm.prank(admin);
        _deliveryManager.adminToggleListingAuthorization(_listingUSDC, true);
    }
}
