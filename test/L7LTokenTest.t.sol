// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import {MultiMinter} from "src/MultiMinter.sol";
import {L7LToken} from "src/token/L7LToken.sol";
import {TestL7LWithDeployments} from "./util/TestL7LWithDeployments.sol";
import {Ownable} from "@openzeppelin/contracts/access/Ownable.sol";
import {Pausable} from "@openzeppelin/contracts/utils/Pausable.sol";

contract L7LTokenTest is TestL7LWithDeployments {
    address internal admin = address(1);
    address internal user = address(2);

    function setUp() public {
        vm.label(admin, "Admin");
        vm.label(user, "User");
        vm.chainId(1);

        // Setup smart contracts
        vm.startPrank(admin);
        deployAll(admin);
        vm.stopPrank();
    }

    function testMintByInvalidUser() public {
        vm.startPrank(user);
        vm.expectRevert(L7LToken.Unauthorized.selector);
        _L7LToken.mint(user, 100);
        vm.stopPrank();
    }

    function testMintByInvalidUserThroughMinter() public {
        vm.startPrank(user);
        vm.expectRevert(MultiMinter.NotMinter.selector);
        _MultiMinter.mint(user, 100);
        assertTrue(_L7LToken.balanceOf(user) == 0);
        assertTrue(_MultiMinter.balanceOf(user) == 0);
        vm.stopPrank();
    }

    function testMintByAdmin() public {
        vm.startPrank(admin);
        vm.expectRevert(L7LToken.Unauthorized.selector);
        _L7LToken.mint(user, 100);
        vm.stopPrank();
    }

    function testMintByAdminThroughMinter() public {
        vm.startPrank(admin);
        _MultiMinter.mint(admin, 100);
        assertTrue(_L7LToken.balanceOf(admin) == 100);
        assertTrue(_MultiMinter.balanceOf(admin) == 100);
        assertTrue(_L7LToken.mintedCanonically() == 100);
        vm.stopPrank();
    }

    function testMintByStaticMinter() public {
        vm.startPrank(address(10));
        vm.expectRevert(L7LToken.Unauthorized.selector);
        _L7LToken.mint(admin, 100);
        vm.stopPrank();

        vm.startPrank(admin);
        assertTrue(_L7LToken.owner() == admin);
        assertTrue(_L7LToken.staticMinter() == address(0));
        vm.expectRevert(L7LToken.Unauthorized.selector);
        _L7LToken.assignStaticMinter(address(0));
        _L7LToken.assignStaticMinter(address(10));
        vm.stopPrank();

        vm.startPrank(address(10));
        _L7LToken.mint(admin, 100);
        assertTrue(_L7LToken.balanceOf(admin) == 100);
        assertTrue(_L7LToken.mintedCanonically() == 100);
        vm.stopPrank();

        vm.startPrank(admin);
        vm.expectRevert(L7LToken.Unauthorized.selector);
        _L7LToken.assignStaticMinter(address(11));
        vm.stopPrank();
    }

    function testMintOnNonCanonicChain() public {
        vm.startPrank(admin);
        vm.chainId(31337);
        vm.expectRevert(L7LToken.OnlyMintOnCanonicChain.selector);
        _MultiMinter.mint(admin, 1);
        vm.stopPrank();
    }

    function testMintOverMaxSupply() public {
        vm.startPrank(admin);
        vm.expectRevert(L7LToken.ReachedMaxSupply.selector);
        _MultiMinter.mint(admin, 1000000001 ether);
        _MultiMinter.mint(admin, 1000000000 ether);
        assertTrue(_L7LToken.mintedCanonically() == 1000000000 ether);
        assertTrue(_L7LToken.balanceOf(admin) == 1000000000 ether);
        vm.expectRevert(L7LToken.ReachedMaxSupply.selector);
        _MultiMinter.mint(admin, 1);
        vm.stopPrank();
    }

    function testMultiMinterManagementByUnauthorisedParty() public {
        vm.startPrank(user);
        vm.expectRevert(abi.encodeWithSelector(Ownable.OwnableUnauthorizedAccount.selector, user));
        _MultiMinter.addMinter(user);
        vm.expectRevert(abi.encodeWithSelector(Ownable.OwnableUnauthorizedAccount.selector, user));
        _MultiMinter.removeMinter(admin);
        vm.stopPrank();
    }

    function testMultiMinterManagement() public {
        vm.startPrank(admin);
        _MultiMinter.addMinter(user);
        assertTrue(_MultiMinter.canMint(user));
        _MultiMinter.removeMinter(user);
        assertFalse(_MultiMinter.canMint(user));
        vm.stopPrank();
    }

    function testMultiMinterPausingByUnauthorisedParty() public {
        vm.startPrank(user);
        vm.expectRevert(abi.encodeWithSelector(Ownable.OwnableUnauthorizedAccount.selector, user));
        _MultiMinter.adminPause();
        vm.expectRevert(abi.encodeWithSelector(Ownable.OwnableUnauthorizedAccount.selector, user));
        _MultiMinter.adminUnpause();
        vm.stopPrank();
    }

    function testMultiMinterPausingUnpausing() public {
        vm.startPrank(admin);
        _MultiMinter.adminPause();
        vm.expectRevert(Pausable.EnforcedPause.selector);
        _MultiMinter.mint(admin, 100);
        _MultiMinter.adminUnpause();
        _MultiMinter.mint(admin, 100);
        assertTrue(_L7LToken.balanceOf(admin) == 100);
        vm.stopPrank();
    }

    function testMultiMinter3rdPartyMinting() public {
        vm.startPrank(admin);
        _MultiMinter.addMinter(user);
        vm.stopPrank();

        vm.startPrank(user);
        _MultiMinter.mint(user, 100);
        assertTrue(_L7LToken.balanceOf(user) == 100);
        vm.stopPrank();
    }

    function testTransferOfOwnership() public {
        vm.startPrank(admin);
        _MultiMinter.transferOwnership(user);
        assertTrue(_MultiMinter.owner() == admin);
        vm.stopPrank();

        vm.startPrank(user);
        _MultiMinter.acceptOwnership();
        assertTrue(_MultiMinter.owner() == user);
        vm.stopPrank();
    }

    function testMigrationToAnotherContractVersion() public {
        vm.startPrank(admin);
        assertTrue(_L7LToken.minter() == address(_MultiMinter));
        _MultiMinter.adminMigrate(admin);
        assertTrue(_L7LToken.minter() == address(_MultiMinter));
        assertTrue(_L7LToken.pendingMinter() == address(admin));
        _L7LToken.acceptMinter();
        assertTrue(_L7LToken.minter() == admin);
        vm.stopPrank();
    }

    function testUnauthorizedMigrationToAnotherContractVersion() public {
        vm.startPrank(user);
        vm.expectRevert(abi.encodeWithSelector(Ownable.OwnableUnauthorizedAccount.selector, user));
        _MultiMinter.adminMigrate(admin);
        vm.stopPrank();
    }

    function testUnauthorizedMigrationToAcceptOwnership() public {
        vm.startPrank(user);
        vm.expectRevert(abi.encodeWithSelector(Ownable.OwnableUnauthorizedAccount.selector, user));
        _MultiMinter.adminAcceptMinter();
        vm.stopPrank();
    }
}
