// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import {Test} from "forge-std/Test.sol";
import {PerkManager} from "src/swap/shop/PerkManager.sol";
import {IPerkManager} from "src/interfaces/IPerkManager.sol";
import {IERC1155} from "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";

contract PerkManagerTest is Test {
    address internal admin = address(1);
    address internal user1 = address(2);
    address internal user2 = address(3);
    address internal user3 = address(4);

    PerkManager public _perkManager;

    function setUp() public {
        vm.label(admin, "Admin");
        vm.label(user1, "User 1");
        vm.chainId(1);

        // Setup smart contracts
        vm.startPrank(admin);
        _perkManager = new PerkManager();
        vm.stopPrank();
    }

    function testNonImplementedFunctions() public {
        vm.startPrank(admin);
        vm.expectRevert(PerkManager.NotImplemented.selector);
        _perkManager.setApprovalForAll(address(3), true);
        assertTrue(_perkManager.isApprovedForAll(address(3), address(3)) == false);
        vm.expectRevert(PerkManager.NotImplemented.selector);
        _perkManager.safeTransferFrom(admin, address(3), 1, 1, "\x00");
        vm.expectRevert(PerkManager.NotImplemented.selector);
        uint256[] memory _ids = new uint256[](1);
        _ids[0] = 1;
        uint256[] memory _amounts = new uint256[](1);
        _amounts[0] = 1;
        _perkManager.safeBatchTransferFrom(admin, address(3), _ids, _amounts, "\x00");
        vm.stopPrank();
    }

    function testImplemenetedInterfaces() public {
        assertTrue(_perkManager.supportsInterface(bytes4(0)) == false);
        assertTrue(_perkManager.supportsInterface(type(IERC1155).interfaceId));
        assertTrue(_perkManager.supportsInterface(type(IPerkManager).interfaceId));
    }

    function testInvalidPerkParams() public {
        vm.startPrank(user1);
        vm.expectRevert(PerkManager.InvalidPerkParams.selector);
        _perkManager.registerPerk(user1, 0, false, abi.encodePacked("{\"name\": \"Perk 1\"}"));
        vm.expectRevert(PerkManager.InvalidPerkParams.selector);
        _perkManager.registerPerk(address(0), 5, false, abi.encodePacked("{\"name\": \"Perk 1\"}"));
        
        uint256 _perkId = _perkManager.registerPerk(user1, 5, false, abi.encodePacked("{\"name\": \"Perk 1\"}"));
        vm.expectRevert(PerkManager.InvalidPerkParams.selector);
        _perkManager.updatePerk(_perkId, user1, 0, false, "\x00");
        vm.stopPrank();
    }

    function testPerkRegistrationAndManagement() public {
        vm.startPrank(user1);
        uint256 _perkId = _regiterSamplePerk(user1);
        _perkManager.updatePerk(_perkId, address(0), 10, false, abi.encodePacked("{\"name\": \"Perk 2\"}"));
        (address _owner, uint256 _totalSupply, uint256 _circulatingSupply, bool _exclusive, bytes memory _metadata) = _perkManager.perks(_perkId);
        assertTrue(_owner == user1);
        assertTrue(_totalSupply == 10);
        assertTrue(_circulatingSupply == 0);
        assertTrue(_exclusive == false);
        assertTrue(keccak256(_metadata) == keccak256(abi.encodePacked("{\"name\": \"Perk 2\"}")));
        _perkManager.updatePerk(_perkId, user2, 10, false, abi.encodePacked("{\"name\": \"Perk 2\"}"));
        (address _newOwner,,,,) = _perkManager.perks(_perkId);
        assertTrue(_newOwner == user2);
        vm.stopPrank();
    }

    function testItsPossibleToSwitchExclusivityOff() public {
        vm.startPrank(user1);
        uint256 _perkId = _perkManager.registerPerk(user1, 5, true, abi.encodePacked("{\"name\": \"Exclusive Perk\"}"));
        (,,, bool _exclusive,) = _perkManager.perks(_perkId);
        assertTrue(_exclusive == true);
        _perkManager.updatePerk(_perkId, address(0), 5, true, "\x00");
        (address _owner,,, bool _updatedExclusive, bytes memory _metadata) = _perkManager.perks(_perkId);
        assertTrue(_owner == user1);
        assertTrue(_updatedExclusive == false);
        assertTrue(keccak256(_metadata) == keccak256(abi.encodePacked("{\"name\": \"Exclusive Perk\"}")));
        _perkManager.updatePerk(_perkId, address(0), 5, false, "\x00");
        (,,, bool _sameExclusive,) = _perkManager.perks(_perkId);
        assertTrue(_sameExclusive == false);
        vm.stopPrank();
    }

    function testNotPossibleToReduceSupplyLowerThanCirculating() public {
        vm.startPrank(user1);
        uint256 _perkId = _perkManager.registerPerk(user1, 5, false, abi.encodePacked("{\"name\": \"Perk 1\"}"));
        _perkManager.grantPerk(_perkId, user2, 5);
        vm.expectRevert(PerkManager.OutOfStock.selector);
        _perkManager.grantPerk(_perkId, user2, 1);
        vm.expectRevert(PerkManager.InvalidPerkParams.selector);
        _perkManager.updatePerk(_perkId, address(0), 4, false, "\x00");
        vm.stopPrank();
    }

    function testRestrictedAccessToPerk() public {
        vm.startPrank(user1);
        uint256 _perkId = _regiterSamplePerk(user1);
        _perkManager.grantPerk(_perkId, user3, 1);
        vm.stopPrank();

        vm.startPrank(user2);
        vm.expectRevert(PerkManager.Unauthorized.selector);
        _perkManager.updatePerk(_perkId, address(0), 10, false, abi.encodePacked("{\"name\": \"Perk 2\"}"));
        vm.expectRevert(PerkManager.Unauthorized.selector);
        _perkManager.grantPerk(_perkId, user3, 1);
        vm.expectRevert(PerkManager.Unauthorized.selector);
        _perkManager.revokePerk(_perkId, user3, 1);
        vm.expectRevert(PerkManager.Unauthorized.selector);
        _perkManager.usePerk(_perkId, user3, keccak256("SMTH"));
        vm.expectRevert(PerkManager.Unauthorized.selector);
        _perkManager.togglePerkManager(_perkId, user3, true);
        vm.stopPrank();

        vm.startPrank(admin);
        vm.expectRevert(PerkManager.Unauthorized.selector);
        _perkManager.updatePerk(_perkId, address(0), 10, false, abi.encodePacked("{\"name\": \"Perk 2\"}"));
        vm.expectRevert(PerkManager.Unauthorized.selector);
        _perkManager.grantPerk(_perkId, user3, 1);
        vm.expectRevert(PerkManager.Unauthorized.selector);
        _perkManager.revokePerk(_perkId, user3, 1);
        vm.expectRevert(PerkManager.Unauthorized.selector);
        _perkManager.usePerk(_perkId, user3, keccak256("SMTH"));
        vm.expectRevert(PerkManager.Unauthorized.selector);
        _perkManager.togglePerkManager(_perkId, user3, true);
        vm.stopPrank();
    }

    function testCanGetPerkMetadata() public {
        vm.startPrank(user1);
        uint256 _perkId = _regiterSamplePerk(user1);
        assertTrue(keccak256(bytes(_perkManager.uri(_perkId))) == keccak256(abi.encodePacked("{\"name\": \"Perk 1\"}")));
        vm.stopPrank();
    }

    function testCanReadPerkBalance() public {
        vm.startPrank(user1);
        uint256 _perkId = _regiterSamplePerk(user1);
        _perkManager.grantPerk(_perkId, user3, 2);
        assertTrue(_perkManager.balanceOf(user3, _perkId) == 2);
        vm.stopPrank();
    }

    function testOwnerCanGrantAndRevokePerkManagers() public {
        vm.startPrank(user1);
        uint256 _perkId = _regiterSamplePerk(user1);
        _perkManager.togglePerkManager(_perkId, user2, true);
        vm.stopPrank();

        vm.startPrank(user2);
        vm.expectRevert(PerkManager.InvalidPerkParams.selector);
        _perkManager.grantPerk(_perkId, user3, 0);
        vm.expectRevert(PerkManager.InvalidPerkParams.selector);
        _perkManager.grantPerk(_perkId, address(0), 1);
        _perkManager.grantPerk(_perkId, user3, 2);
        _perkManager.revokePerk(_perkId, user3, 1);
        _perkManager.usePerk(_perkId, user3, keccak256("TEST"));
        assertTrue(_perkManager.balanceOf(user3, _perkId) == 1);
        vm.stopPrank();

        vm.startPrank(user1);
        _perkManager.togglePerkManager(_perkId, user2, false);
        vm.stopPrank();

        vm.startPrank(user2);
        vm.expectRevert(PerkManager.Unauthorized.selector);
        _perkManager.grantPerk(_perkId, user3, 2);
        vm.expectRevert(PerkManager.Unauthorized.selector);
        _perkManager.revokePerk(_perkId, user3, 1);
        vm.expectRevert(PerkManager.Unauthorized.selector);
        _perkManager.usePerk(_perkId, user3, keccak256("TEST"));
        vm.stopPrank();
    }

    function testExclusivePerkCanBeGrantedOnce() public {
        vm.startPrank(user1);
        uint256 _perkId = _perkManager.registerPerk(user1, 5, true, abi.encodePacked("{\"name\": \"Perk 1\"}"));
        vm.expectRevert(PerkManager.OnePerWallet.selector);
        _perkManager.grantPerk(_perkId, user2, 2);
        _perkManager.grantPerk(_perkId, user2, 1);
        vm.expectRevert(PerkManager.OnePerWallet.selector);
        _perkManager.grantPerk(_perkId, user2, 1);
        _perkManager.revokePerk(_perkId, user2, 1);
        _perkManager.grantPerk(_perkId, user2, 1);
        vm.stopPrank();
    }

    function testPerkUsage() public {
        vm.prank(user1);
        uint256 _perkId = _regiterSamplePerk(user1);

        vm.startPrank(user2);
        vm.expectRevert(PerkManager.NotEnoughPerks.selector);
        _perkManager.usePerk(_perkId, user2, keccak256("TEST"));
        vm.stopPrank();

        vm.prank(user1);
        _perkManager.grantPerk(_perkId, user2, 1);

        vm.startPrank(user2);
        (uint256 _usages, uint256 _totalUsages) = _perkManager.usePerk(_perkId, user2, keccak256("TEST"));
        assertTrue(_usages == 1);
        assertTrue(_totalUsages == 1);
        (uint256 _otherUsages, uint256 _updatedTotalUsages) = _perkManager.usePerk(_perkId, user2, keccak256("TEST2"));
        assertTrue(_otherUsages == 1);
        assertTrue(_updatedTotalUsages == 2);
        assertTrue(_perkManager.perkUsages(_perkId, user2, keccak256("TEST2")) == 1);
        assertTrue(_perkManager.perkTotalUsages(_perkId, user2) == 2);
        vm.stopPrank();

        vm.startPrank(user3);
        vm.expectRevert(PerkManager.Unauthorized.selector);
        _perkManager.usePerk(_perkId, user2, keccak256("TEST3"));
        vm.stopPrank();

        vm.startPrank(user1);
        _perkManager.usePerk(_perkId, user2, keccak256("TEST3"));
        assertTrue(_perkManager.perkTotalUsages(_perkId, user2) == 3);
        vm.stopPrank();
    }

    function testPerkRevokation() public {
        vm.prank(user1);
        uint256 _perkId = _regiterSamplePerk(user1);

        vm.startPrank(user2);
        vm.expectRevert(PerkManager.NotEnoughPerks.selector);
        _perkManager.revokePerk(_perkId, user2, 1);
        vm.stopPrank();

        vm.prank(user1);
        _perkManager.grantPerk(_perkId, user2, 2);

        vm.startPrank(user2);
        vm.expectRevert(PerkManager.InvalidPerkParams.selector);
        _perkManager.revokePerk(_perkId, user2, 0);
        _perkManager.revokePerk(_perkId, user2, 1);
        assertTrue(_perkManager.balanceOf(user2, _perkId) == 1);
        address[] memory _users = new address[](2);
        _users[0] = user1;
        _users[1] = user2;
        uint256[] memory _perks = new uint256[](2);
        _perks[0] = _perkId;
        _perks[1] = _perkId;
        uint256[] memory _match = new uint256[](2);
        _match[0] = 0;
        _match[1] = 1;
        assertTrue(keccak256(abi.encodePacked(_perkManager.balanceOfBatch(_users, _perks))) == keccak256(abi.encodePacked(_match)));
        vm.stopPrank();

        vm.startPrank(user3);
        vm.expectRevert(PerkManager.Unauthorized.selector);
        _perkManager.revokePerk(_perkId, user2, 1);
        vm.stopPrank();

        vm.startPrank(user1);
        _perkManager.revokePerk(_perkId, user2, 1);
        assertTrue(_perkManager.balanceOf(user2, _perkId) == 0);
        vm.expectRevert(PerkManager.NotEnoughPerks.selector);
        _perkManager.revokePerk(_perkId, user2, 1);
        vm.stopPrank();
    }

    function _regiterSamplePerk(address _owner) internal returns(uint256) {
        return _perkManager.registerPerk(_owner, 5, false, abi.encodePacked("{\"name\": \"Perk 1\"}"));
    }
}
