// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import {MultiMinter} from "src/MultiMinter.sol";
import {EESeedToken} from "src/token/EESeedToken.sol";
import {RedeemSeedByMint} from "src/vesting/RedeemSeedByMint.sol";
import {TestL7LWithDeployments} from "./util/TestL7LWithDeployments.sol";
import {IERC20} from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import {Ownable} from "@openzeppelin/contracts/access/Ownable.sol";
import {Pausable} from "@openzeppelin/contracts/utils/Pausable.sol";

contract RedeemSeedTest is TestL7LWithDeployments {
    address internal admin = address(1);
    address internal user = address(2);
    address internal replaced_user = address(3);

    EESeedToken internal _EESeedToken;
    RedeemSeedByMint internal _redemptionContract;

    function setUp() public {
        vm.label(admin, "Admin");
        vm.label(user, "User");
        vm.chainId(1);

        // Setup smart contracts
        vm.startPrank(admin);
        deployAll(admin);
        _EESeedToken = new EESeedToken(admin);
        _redemptionContract = new RedeemSeedByMint(
            admin,
            IERC20(address(_EESeedToken)),
            IERC20(address(_MultiMinter)),
            1,
            1,
            block.timestamp,
            block.timestamp + 1000,
            1000000000 ether
        );
        _MultiMinter.addMinter(address(_redemptionContract));
        vm.stopPrank();
    }

    function testInvalidDeploymentStartBeforeEnd() public {
        vm.expectRevert(RedeemSeedByMint.InvalidVestingTime.selector);
        new RedeemSeedByMint(
            admin,
            IERC20(address(_EESeedToken)),
            IERC20(address(_MultiMinter)),
            1,
            1,
            block.timestamp + 1,
            block.timestamp,
            1000
        );
    }

    function testInvalidDeploymentStartInAdvance() public {
        vm.expectRevert(RedeemSeedByMint.InvalidVestingTime.selector);
        new RedeemSeedByMint(
            admin,
            IERC20(address(_EESeedToken)),
            IERC20(address(_MultiMinter)),
            1,
            1,
            block.timestamp - 1,
            block.timestamp + 10,
            1000
        );
    }

    function testPausingUnpausingByUnauthorisedParty() public {
        vm.startPrank(user);
        vm.expectRevert(abi.encodeWithSelector(Ownable.OwnableUnauthorizedAccount.selector, user));
        _redemptionContract.adminPause();
        vm.expectRevert(abi.encodeWithSelector(Ownable.OwnableUnauthorizedAccount.selector, user));
        _redemptionContract.adminUnpause();
        vm.stopPrank();
    }

    function testVestingScheduleForPausedClaims() public {
        vm.startPrank(admin);
        _EESeedToken.grantRole(_EESeedToken.MINTER_ROLE(), admin);
        _EESeedToken.mint(user, 100);
        _redemptionContract.adminPause();
        vm.stopPrank();

        vm.warp(block.timestamp + 750);

        vm.startPrank(user);
        assertTrue(_redemptionContract.vestedAmountToClaim(user) == 75);
        vm.expectRevert(Pausable.EnforcedPause.selector);
        _redemptionContract.claim();
        vm.stopPrank();

        vm.prank(admin);
        _redemptionContract.adminUnpause();

        vm.startPrank(user);
        _redemptionContract.claim();
        assertTrue(_L7LToken.balanceOf(user) == 75);
        vm.stopPrank();
    }

    function testVestingScheduleForNoHolder() public {
        vm.startPrank(user);
        assertTrue(_redemptionContract.vestedAmountToClaim(user) == 0);
        vm.expectRevert(RedeemSeedByMint.NoClaim.selector);
        _redemptionContract.claim();
        vm.stopPrank();
    }

    function testVestingScheduleForHolderEmpty() public {
        vm.startPrank(admin);
        _EESeedToken.grantRole(_EESeedToken.MINTER_ROLE(), admin);
        _EESeedToken.mint(user, 100);
        vm.stopPrank();

        vm.startPrank(user);
        assertTrue(_redemptionContract.vestedAmountToClaim(user) == 0);
        vm.expectRevert(RedeemSeedByMint.NothingToClaim.selector);
        _redemptionContract.claim();
        vm.stopPrank();
    }

    function testVestingScheduleNotStarted() public {
        vm.startPrank(admin);
        _EESeedToken.grantRole(_EESeedToken.MINTER_ROLE(), admin);
        _EESeedToken.mint(user, 100);
        vm.stopPrank();

        vm.warp(block.timestamp - 1);

        vm.startPrank(user);
        assertTrue(_redemptionContract.vestedAmountToClaim(user) == 0);
        vm.stopPrank();
    }

    function testVestingScheduleForHolderTwoThirds() public {
        vm.startPrank(admin);
        _EESeedToken.grantRole(_EESeedToken.MINTER_ROLE(), admin);
        _EESeedToken.mint(user, 100);
        vm.stopPrank();

        vm.warp(block.timestamp + 750);

        vm.startPrank(user);
        assertTrue(_redemptionContract.vestedAmountToClaim(user) == 75);
        _redemptionContract.claim();
        assertTrue(_L7LToken.balanceOf(user) == 75);
        assertTrue(_redemptionContract.vestedAmountToClaim(user) == 0);
        vm.expectRevert(RedeemSeedByMint.NothingToClaim.selector);
        _redemptionContract.claim();
        vm.stopPrank();
    }

    function testVestingScheduleForHolderFullyVested() public {
        vm.startPrank(admin);
        _EESeedToken.grantRole(_EESeedToken.MINTER_ROLE(), admin);
        _EESeedToken.mint(user, 100);
        vm.stopPrank();

        vm.warp(block.timestamp + 1100);

        vm.startPrank(user);
        assertTrue(_redemptionContract.vestedAmountToClaim(user) == 100);
        _redemptionContract.claim();
        assertTrue(_L7LToken.balanceOf(user) == 100);

        vm.warp(block.timestamp + 2000);

        assertTrue(_redemptionContract.vestedAmountToClaim(user) == 0);
        vm.expectRevert(RedeemSeedByMint.NothingToClaim.selector);
        _redemptionContract.claim();
        vm.stopPrank();
    }

    function testVestingScheduleForSeveralClaims() public {
        vm.startPrank(admin);
        _EESeedToken.grantRole(_EESeedToken.MINTER_ROLE(), admin);
        _EESeedToken.mint(user, 100);
        vm.stopPrank();

        uint256 _ts = block.timestamp;
        assert(_ts == 1);
        vm.warp(101);

        vm.startPrank(user);
        assertTrue(_redemptionContract.vestedAmountToClaim(user) == 10);
        _redemptionContract.claim();
        assertTrue(_L7LToken.balanceOf(user) == 10);

        vm.warp(301);

        assertTrue(_redemptionContract.vestedAmountToClaim(user) == 20);
        _redemptionContract.claim();
        assertTrue(_L7LToken.balanceOf(user) == 30);

        vm.warp(801);

        assertTrue(_redemptionContract.vestedAmountToClaim(user) == 50);
        _redemptionContract.claim();
        assertTrue(_L7LToken.balanceOf(user) == 80);

        vm.warp(1001);

        assertTrue(_redemptionContract.vestedAmountToClaim(user) == 20);
        _redemptionContract.claim();
        assertTrue(_L7LToken.balanceOf(user) == 100);

        vm.warp(1101);
        assertTrue(_redemptionContract.vestedAmountToClaim(user) == 0);
        vm.stopPrank();
    }

    function testNotExceedMintCap() public {
        vm.startPrank(admin);
        _EESeedToken.grantRole(_EESeedToken.MINTER_ROLE(), admin);
        _EESeedToken.mint(user, 1100000000 ether);
        vm.stopPrank();

        vm.warp(1001);

        vm.startPrank(user);
        assertTrue(_redemptionContract.vestedAmountToClaim(user) == 1100000000 ether);
        vm.expectRevert(RedeemSeedByMint.ReachedMintCap.selector);
        _redemptionContract.claim();
        vm.stopPrank();
    }

    function testOnlyAdminCanMarkWalletCompomised() public {
        vm.startPrank(admin);
        _EESeedToken.grantRole(_EESeedToken.MINTER_ROLE(), admin);
        _EESeedToken.mint(user, 100);
        vm.stopPrank();

        vm.startPrank(user);
        vm.expectRevert(abi.encodeWithSelector(Ownable.OwnableUnauthorizedAccount.selector, user));
        _redemptionContract.adminCompromisedWallet(user, replaced_user, bytes(abi.encode("test")));
        vm.stopPrank();
    }

    function testCompomisedWalletSwap() public {
        vm.startPrank(admin);
        _EESeedToken.grantRole(_EESeedToken.MINTER_ROLE(), admin);
        _EESeedToken.mint(user, 100);
        _redemptionContract.adminCompromisedWallet(user, replaced_user, bytes(abi.encode("test")));
        vm.stopPrank();

        uint256 _ts = block.timestamp;
        assert(_ts == 1);
        vm.warp(101);

        vm.startPrank(replaced_user);
        _redemptionContract.claim(user);
        assertTrue(_L7LToken.balanceOf(replaced_user) == 10);
        vm.stopPrank();

        vm.warp(301);

        vm.startPrank(replaced_user);
        _redemptionContract.claim(user);
        assertTrue(_L7LToken.balanceOf(replaced_user) == 30);
        vm.stopPrank();
    }

    function testCompomisedWalletSwapByCompromiser() public {
        vm.startPrank(admin);
        _EESeedToken.grantRole(_EESeedToken.MINTER_ROLE(), admin);
        _EESeedToken.mint(user, 100);
        _redemptionContract.adminCompromisedWallet(user, replaced_user, bytes(abi.encode("test")));
        vm.stopPrank();

        uint256 _ts = block.timestamp;
        vm.warp(_ts + 100);

        vm.startPrank(user);
        vm.expectRevert(RedeemSeedByMint.Unauthorized.selector);
        _redemptionContract.claim(user);
        vm.stopPrank();

        vm.startPrank(address(4));
        vm.expectRevert(RedeemSeedByMint.Unauthorized.selector);
        _redemptionContract.claim(user);
        vm.stopPrank();
    }
}
