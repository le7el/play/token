// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import {Ownable} from "@openzeppelin/contracts/access/Ownable.sol";
import {IERC20Errors} from "@openzeppelin/contracts/interfaces/draft-IERC6093.sol";
import {Pausable} from "@openzeppelin/contracts/utils/Pausable.sol";
import {IAccessControl} from "@openzeppelin/contracts/access/IAccessControl.sol";
import {TestShopWithDeployments} from "./util/TestShopWithDeployments.sol";
import {IListing} from "src/interfaces/IListing.sol";
import {IPaymentManager} from "src/interfaces/IPaymentManager.sol";
import {IDeliveryManager} from "src/interfaces/IDeliveryManager.sol";
import {Shop} from "src/swap/Shop.sol";
import {PerkManager} from "src/swap/shop/PerkManager.sol";
import {PaymentManager} from "src/swap/shop/PaymentManager.sol";
import {PerkDeliveryManager} from "src/swap/shop/PerkDeliveryManager.sol";
import {ERC20DeliveryManager} from "src/swap/shop/ERC20DeliveryManager.sol";
import {MockERC1155} from "solmate/test/utils/mocks/MockERC1155.sol";
import {MockERC20} from "solmate/test/utils/mocks/MockERC20.sol";

contract ShopTest is TestShopWithDeployments {
    address internal admin = address(1);
    address internal seller1 = address(2);
    address internal seller2 = address(6);
    address internal buyer1 = address(3);
    address internal buyer2 = address(4);
    address internal buyer3 = address(5);

    MockERC1155 mockERC1155 = new MockERC1155();
    MockERC20 mockERC20 = new MockERC20("COIN", "COIN", 18);

    function setUp() public {
        vm.label(admin, "Admin");
        vm.label(seller1, "Seller 1");
        vm.label(buyer1, "Buyer 1");
        vm.label(buyer2, "Buyer 2");
        vm.chainId(1);

        // Setup smart contracts
        vm.startPrank(admin);
        deployAll(admin, seller1);
        _MultiMinter.mint(buyer1, 1000 ether);
        vm.stopPrank();
    }

    function testGettingQuotes() public {
        vm.startPrank(seller1);
        _shop.list(
            _soldPerk1,
            IListing.PaymentMethod(address(0), 0, IListing.PaymentType.GAS_COIN_TRANSFER, admin, address(0)),
            IListing.Ware(address(mockERC20), 0, IListing.WareType.ERC20_MINT, 100000000 ether, 1, 58888, address(_deliveryManager))
        );
        vm.stopPrank();

        (uint256 _quote,,) = _shop.quoteFor(_soldPerk1, msg.sender, 1 ether, "\x00");
        assertTrue(_quote == 16981388398315);
        (uint256 _offer1,,) = _shop.offerOf(_soldPerk1, msg.sender, 0.1 ether, "\x00");
        assertTrue(_offer1 == 5888.8 ether);
        (uint256 _offer2,,) = _shop.offerOf(_soldPerk1, msg.sender, 16981388398315, "\x00");
        assertTrue(_offer2 == 999999999999973720); // precision loss, about 1 ether
    }

    function testBuyPerkFromShop() public {
        _defaultListing();
        
        vm.startPrank(buyer1);
        assertTrue(_perkManager.balanceOf(buyer1, _exclusePerk) == 0);
        _L7LToken.approve(address(_paymentManager), 100 ether);
        _shop.exchange(_soldPerk1, 100 ether, 1, "\x00");
        assertTrue(_perkManager.balanceOf(buyer1, _exclusePerk) == 1);
        assertTrue(_L7LToken.balanceOf(buyer1) == 900 ether);
        assertTrue(_L7LToken.balanceOf(seller1) == 100 ether);
        vm.stopPrank();
    }

    function testBuyPerkFromShopWithERC20Holding() public {
        vm.startPrank(seller1);
        _shop.list(
            _soldPerk1,
            IListing.PaymentMethod(address(_L7LToken), 0, IListing.PaymentType.ERC20_HOLD, seller1, address(0)),
            IListing.Ware(address(_perkManager), _exclusePerk, IListing.WareType.PERK_EXCLUSIVE, 5, 10, 1, address(_deliveryManager))
        );
        vm.stopPrank();

        vm.prank(admin);
        _deliveryManager.adminToggleListingAuthorization(_soldPerk1, true);
        
        vm.startPrank(buyer2);
        vm.expectRevert(IPaymentManager.PaymentFailed.selector);
        _shop.exchange(_soldPerk1, 10, 1, "\x00");
        assertTrue(_perkManager.balanceOf(buyer2, _exclusePerk) == 0);
        vm.stopPrank();

        vm.prank(admin);
        _MultiMinter.mint(buyer2, 10);

        vm.startPrank(buyer2);
        _shop.exchange(_soldPerk1, 10, 1, "\x00");
        assertTrue(_perkManager.balanceOf(buyer2, _exclusePerk) == 1);
        assertTrue(_L7LToken.balanceOf(buyer2) == 10);
        assertTrue(_L7LToken.balanceOf(seller1) == 0);
        vm.expectRevert(PerkManager.OnePerWallet.selector);
        _shop.exchange(_soldPerk1, 10, 1, "\x00");
        vm.stopPrank();
    }

    function testBuyPerkFromShopWithERC1155() public {
        vm.startPrank(seller1);
        _shop.list(
            _soldPerk1,
            IListing.PaymentMethod(address(mockERC1155), 1, IListing.PaymentType.ERC1155_TRANSFER, seller1, address(0)),
            IListing.Ware(address(_perkManager), _exclusePerk, IListing.WareType.PERK_EXCLUSIVE, 5, 10, 1, address(_deliveryManager))
        );
        vm.stopPrank();

        vm.prank(admin);
        _deliveryManager.adminToggleListingAuthorization(_soldPerk1, true);
        
        vm.startPrank(buyer1);
        assertTrue(_perkManager.balanceOf(buyer1, _exclusePerk) == 0);
        mockERC1155.mint(buyer1, 1, 10, "\x00");
        mockERC1155.setApprovalForAll(address(_paymentManager), true);
        _shop.exchange(_soldPerk1, 10, 1, "\x00");
        assertTrue(_perkManager.balanceOf(buyer1, _exclusePerk) == 1);
        assertTrue(mockERC1155.balanceOf(buyer1, 1) == 0);
        assertTrue(mockERC1155.balanceOf(seller1, 1) == 10);
        vm.stopPrank();
    }

    function testBuyPerkFromShopWithERC1155Holding() public {
        vm.startPrank(seller1);
        _shop.list(
            _soldPerk1,
            IListing.PaymentMethod(address(mockERC1155), 1, IListing.PaymentType.ERC1155_HOLD, seller1, address(0)),
            IListing.Ware(address(_perkManager), _exclusePerk, IListing.WareType.PERK_EXCLUSIVE, 5, 10, 1, address(_deliveryManager))
        );
        vm.stopPrank();

        vm.prank(admin);
        _deliveryManager.adminToggleListingAuthorization(_soldPerk1, true);
        
        vm.startPrank(buyer1);
        vm.expectRevert(IPaymentManager.PaymentFailed.selector);
        _shop.exchange(_soldPerk1, 10, 1, "\x00");
        assertTrue(_perkManager.balanceOf(buyer1, _exclusePerk) == 0);
        mockERC1155.mint(buyer1, 1, 10, "\x00");
        _shop.exchange(_soldPerk1, 10, 1, "\x00");
        assertTrue(_perkManager.balanceOf(buyer1, _exclusePerk) == 1);
        assertTrue(mockERC1155.balanceOf(buyer1, 1) == 10);
        assertTrue(mockERC1155.balanceOf(seller1, 1) == 0);
        vm.expectRevert(PerkManager.OnePerWallet.selector);
        _shop.exchange(_soldPerk1, 10, 1, "\x00");
        vm.stopPrank();
    }

    function testBuyPerkFromShopWithERC1155Burn() public {
        vm.startPrank(seller1);
        _shop.list(
            _soldPerk1,
            IListing.PaymentMethod(address(mockERC1155), 1, IListing.PaymentType.ERC1155_BURN, seller1, address(0)),
            IListing.Ware(address(_perkManager), _exclusePerk, IListing.WareType.PERK_EXCLUSIVE, 5, 10, 1, address(_deliveryManager))
        );
        vm.stopPrank();

        vm.prank(admin);
        _deliveryManager.adminToggleListingAuthorization(_soldPerk1, true);
        
        vm.startPrank(buyer1);
        assertTrue(_perkManager.balanceOf(buyer1, _exclusePerk) == 0);
        mockERC1155.mint(buyer1, 1, 10, "\x00");
        mockERC1155.setApprovalForAll(address(_paymentManager), true);
        _shop.exchange(_soldPerk1, 10, 1, "\x00");
        assertTrue(_perkManager.balanceOf(buyer1, _exclusePerk) == 1);
        assertTrue(mockERC1155.balanceOf(buyer1, 1) == 0);
        assertTrue(mockERC1155.balanceOf(seller1, 1) == 0);
        vm.stopPrank();
    }

    function testBuyTokenForPerkFromShop() public {
        vm.startPrank(admin);
        ERC20DeliveryManager _erc20DeliveryManager = new ERC20DeliveryManager(address(seller1), address(_shop));
        _perkManager.grantPerk(_exclusePerk, buyer2, 1);
        _MultiMinter.mint(address(_erc20DeliveryManager), 1000);
        _perkManager.togglePerkManager(_exclusePerk, address(_paymentManager), true);
        vm.stopPrank();
        
        vm.startPrank(seller1);
        _erc20DeliveryManager.adminToggleWareAddressAuthorization(address(_L7LToken), true);
        _shop.list(
            _soldPerk1,
            IListing.PaymentMethod(address(_perkManager), _exclusePerk, IListing.PaymentType.PERK_REVOKE, seller1, address(0)),
            IListing.Ware(address(_L7LToken), 0, IListing.WareType.ERC20_TRANSFER, 100000, 1, 1000, address(_erc20DeliveryManager))
        );
        _erc20DeliveryManager.adminToggleListingAuthorization(_soldPerk1, true);
        vm.stopPrank();
        
        vm.startPrank(buyer2);
        assertTrue(_perkManager.balanceOf(buyer2, _exclusePerk) == 1);
        _shop.exchange(_soldPerk1, 1, 1000, "\x00");
        assertTrue(_perkManager.balanceOf(buyer2, _exclusePerk) == 0);
        assertTrue(_L7LToken.balanceOf(buyer2) == 1000);
        assertTrue(_L7LToken.balanceOf(address(_erc20DeliveryManager)) == 0);
        vm.stopPrank();
    }

    function testDelisting() public {
        _defaultListing();

        vm.startPrank(buyer1);
        assertTrue(_perkManager.balanceOf(buyer1, _exclusePerk) == 0);
        _L7LToken.approve(address(_paymentManager), 100 ether);
        _shop.exchange(_soldPerk1, 100 ether, 1, "\x00");
        assertTrue(_perkManager.balanceOf(buyer1, _exclusePerk) == 1);
        assertTrue(_L7LToken.balanceOf(buyer1) == 900 ether);
        assertTrue(_L7LToken.balanceOf(seller1) == 100 ether);
        vm.stopPrank();

        vm.startPrank(admin);
        _MultiMinter.mint(buyer2, 100 ether);
        vm.stopPrank();

        vm.startPrank(seller1);
        _shop.delist(_soldPerk1);
        vm.stopPrank();

        vm.startPrank(buyer2);
        _L7LToken.approve(address(_paymentManager), 100 ether);
        vm.expectRevert(Shop.InvalidListing.selector);
        _shop.exchange(_soldPerk1, 100 ether, 1, "\x00");
        vm.stopPrank();
    }

    function testPerkExclusivity() public {
        _defaultListing();
        
        vm.startPrank(buyer1);
        assertTrue(_perkManager.balanceOf(buyer1, _exclusePerk) == 0);
        _L7LToken.approve(address(_paymentManager), 200 ether);
        _shop.exchange(_soldPerk1, 100 ether, 1, "\x00");
        assertTrue(_perkManager.balanceOf(buyer1, _exclusePerk) == 1);
        vm.expectRevert(PerkManager.OnePerWallet.selector);
        _shop.exchange(_soldPerk1, 100 ether, 1, "\x00");
        vm.stopPrank();
    }

    function testRevertWhenBuyingWithoutToken() public {
        _defaultListing();
        
        vm.startPrank(buyer3);
        assertTrue(_L7LToken.balanceOf(buyer3) == 0);
        _L7LToken.approve(address(_paymentManager), 100 ether);
        vm.expectRevert(abi.encodeWithSelector(IERC20Errors.ERC20InsufficientBalance.selector, buyer3, 0, 100 ether));
        _shop.exchange(_soldPerk1, 100 ether, 1, "\x00");
        assertTrue(_perkManager.balanceOf(buyer1, _exclusePerk) == 0);
        vm.stopPrank();
    }

    function testPauseByAdmin() public {
        _defaultListing();

        vm.startPrank(admin);
        _shop.adminToggleTradesAndListings(true);
        vm.stopPrank();
        
        vm.startPrank(buyer1);
        _L7LToken.approve(address(_paymentManager), 100 ether);
        vm.expectRevert(Pausable.EnforcedPause.selector);
        _shop.exchange(_soldPerk1, 100 ether, 1, "\x00");
        assertTrue(_perkManager.balanceOf(buyer1, _exclusePerk) == 0);
        vm.stopPrank();

        vm.startPrank(admin);
        _shop.adminToggleTradesAndListings(false);
        vm.stopPrank();

        vm.startPrank(buyer1);
        _L7LToken.approve(address(_paymentManager), 100 ether);
        _shop.exchange(_soldPerk1, 100 ether, 1, "\x00");
        assertTrue(_perkManager.balanceOf(buyer1, _exclusePerk) == 1);
        vm.stopPrank();
    }

    function testOnlyAdminCanTogglePause() public {
        _defaultListing();

        vm.startPrank(seller1);
        vm.expectRevert(abi.encodeWithSelector(IAccessControl.AccessControlUnauthorizedAccount.selector, seller1, _shop.DEFAULT_ADMIN_ROLE()));
        _shop.adminToggleTradesAndListings(true);
        vm.expectRevert(abi.encodeWithSelector(IAccessControl.AccessControlUnauthorizedAccount.selector, seller1, _shop.DEFAULT_ADMIN_ROLE()));
        _shop.adminToggleTradesAndListings(false);
        vm.stopPrank();
    }

    function testAdminCanSetFees() public {
        vm.startPrank(admin);
        _shop.adminSetListingFee(0.0002 ether);
        _shop.adminSetExchangeFee(0.0001 ether);
        vm.stopPrank();

        vm.startPrank(seller1);
        vm.expectRevert(Shop.ComissionFailed.selector);
        _shop.list(
            _soldPerk1,
            IListing.PaymentMethod(address(_L7LToken), 0, IListing.PaymentType.ERC20_TRANSFER, seller1, address(0)),
            IListing.Ware(address(_perkManager), _exclusePerk, IListing.WareType.PERK_EXCLUSIVE, 5, 100 ether, 1, address(_deliveryManager))
        );
        vm.deal(seller1, 0.0002 ether);
        _shop.list{value: 0.0002 ether}(
            _soldPerk1,
            IListing.PaymentMethod(address(_L7LToken), 0, IListing.PaymentType.ERC20_TRANSFER, seller1, address(0)),
            IListing.Ware(address(_perkManager), _exclusePerk, IListing.WareType.PERK_EXCLUSIVE, 5, 100 ether, 1, address(_deliveryManager))
        );
        vm.stopPrank();

        vm.prank(admin);
        _deliveryManager.adminToggleListingAuthorization(_soldPerk1, true);

        vm.startPrank(buyer1);
        assertTrue(_perkManager.balanceOf(buyer1, _exclusePerk) == 0);
        _L7LToken.approve(address(_paymentManager), 100 ether);
        vm.expectRevert(Shop.ComissionFailed.selector);
        _shop.exchange(_soldPerk1, 100 ether, 1, "\x00");
        vm.deal(buyer1, 0.0001 ether);
        _shop.exchange{value: 0.0001 ether}(_soldPerk1, 100 ether, 1, "\x00");
        assertTrue(_perkManager.balanceOf(buyer1, _exclusePerk) == 1);
        assertTrue(_L7LToken.balanceOf(buyer1) == 900 ether);
        assertTrue(_L7LToken.balanceOf(seller1) == 100 ether);
        vm.stopPrank();

        assertTrue(admin.balance == 0.0003 ether);
    }

    function testAdminCanSetCustomExchangeFeesForListing() public {
        // Custom fee
        vm.startPrank(admin);
        uint256 _perk = _perkManager.registerPerk(admin, 1000, false, "\x00");
        _perkManager.togglePerkManager(_perk, address(_deliveryManager), true);
        _shop.adminSetCustomExchangeFee(_soldPerk1, 0.0003 ether);
        vm.stopPrank();

        vm.startPrank(seller1);
        _shop.list(
            _soldPerk1,
            IListing.PaymentMethod(address(_L7LToken), 0, IListing.PaymentType.ERC20_TRANSFER, seller1, address(0)),
            IListing.Ware(address(_perkManager), _perk, IListing.WareType.PERK, 5, 100 ether, 1, address(_deliveryManager))
        );
        vm.stopPrank();

        vm.prank(admin);
        _deliveryManager.adminToggleListingAuthorization(_soldPerk1, true);

        vm.startPrank(buyer1);
        assertTrue(_perkManager.balanceOf(buyer1, _perk) == 0);
        _L7LToken.approve(address(_paymentManager), 100 ether);
        vm.expectRevert(Shop.ComissionFailed.selector);
        _shop.exchange(_soldPerk1, 100 ether, 1, "\x00");
        vm.deal(buyer1, 0.0003 ether);
        _shop.exchange{value: 0.0003 ether}(_soldPerk1, 100 ether, 1, "\x00");
        assertTrue(_perkManager.balanceOf(buyer1, _perk) == 1);
        assertTrue(_L7LToken.balanceOf(buyer1) == 900 ether);
        assertTrue(_L7LToken.balanceOf(seller1) == 100 ether);
        vm.stopPrank();

        // No fee for specific listing
        vm.startPrank(admin);
        _shop.adminSetExchangeFee(0.0001 ether);
        _shop.adminSetCustomExchangeFee(_soldPerk1, 1);
        vm.stopPrank();

        vm.startPrank(buyer1);
        _L7LToken.approve(address(_paymentManager), 100 ether);
        _shop.exchange(_soldPerk1, 100 ether, 1, "\x00");
        vm.stopPrank();

        // Reset to default
        vm.startPrank(admin);
        _shop.adminSetCustomExchangeFee(_soldPerk1, 0);
        vm.stopPrank();

        vm.startPrank(buyer1);
        vm.deal(buyer1, 0.0001 ether);
        _L7LToken.approve(address(_paymentManager), 100 ether);
        _shop.exchange{value: 0.0001 ether}(_soldPerk1, 100 ether, 1, "\x00");
        vm.stopPrank();

        assertTrue(admin.balance == 0.0004 ether);
    }

    function testOnlyAdminCanSetFees() public {
        vm.startPrank(seller1);
        vm.expectRevert(abi.encodeWithSelector(IAccessControl.AccessControlUnauthorizedAccount.selector, seller1, _shop.DEFAULT_ADMIN_ROLE()));
        _shop.adminSetListingFee(0.0002 ether);
        vm.expectRevert(abi.encodeWithSelector(IAccessControl.AccessControlUnauthorizedAccount.selector, seller1, _shop.DEFAULT_ADMIN_ROLE()));
        _shop.adminSetExchangeFee(0.0001 ether);
        vm.expectRevert(abi.encodeWithSelector(IAccessControl.AccessControlUnauthorizedAccount.selector, seller1, _shop.DEFAULT_ADMIN_ROLE()));
        _shop.adminSetCustomExchangeFee(_soldPerk1, 0.0001 ether);
        vm.stopPrank();
    }

    function testOnlyAdminCanChangeMiscConfigSettings() public {
        vm.startPrank(seller1);
        PaymentManager _paymentManager = new PaymentManager(address(_shop));
        vm.expectRevert(abi.encodeWithSelector(IAccessControl.AccessControlUnauthorizedAccount.selector, seller1, _shop.DEFAULT_ADMIN_ROLE()));
        _shop.adminChangeDefaultPaymentManager(_paymentManager);
        vm.expectRevert(abi.encodeWithSelector(IAccessControl.AccessControlUnauthorizedAccount.selector, seller1, _shop.DEFAULT_ADMIN_ROLE()));
        _shop.adminRestrictListing(false);
        vm.stopPrank();
    }

    function testAdminCanEnablePublicListing() public {
        vm.startPrank(seller2);
        vm.expectRevert(Shop.Unauthorized.selector);
        _shop.list(
            _soldPerk1,
            IListing.PaymentMethod(address(_L7LToken), 0, IListing.PaymentType.ERC20_TRANSFER, seller2, address(0)),
            IListing.Ware(address(_perkManager), _exclusePerk, IListing.WareType.PERK_EXCLUSIVE, 5, 100 ether, 1, address(_deliveryManager))
        );
        vm.stopPrank();
        
        vm.startPrank(admin);
        _shop.adminRestrictListing(false);
        _deliveryManager.adminToggleListingAuthorization(_soldPerk1, true);
        vm.stopPrank();

        vm.startPrank(seller2);
        _shop.list(
            _soldPerk1,
            IListing.PaymentMethod(address(_L7LToken), 0, IListing.PaymentType.ERC20_TRANSFER, seller2, address(0)),
            IListing.Ware(address(_perkManager), _exclusePerk, IListing.WareType.PERK_EXCLUSIVE, 5, 100 ether, 1, address(_deliveryManager))
        );
        vm.stopPrank();
    }

    function testAdminCanChangeDefaultPaymentManager() public {
        vm.startPrank(admin);
        PaymentManager _paymentManager = new PaymentManager(address(_shop));
        _shop.adminChangeDefaultPaymentManager(_paymentManager);
        assertTrue(address(_shop.defaultPaymentManager()) == address(_paymentManager));
        vm.stopPrank();
    }

    function testSellerCanUpdateHisListing() public {
        _defaultListing();

        vm.startPrank(seller1);
        PaymentManager _paymentManager = new PaymentManager(address(_shop));
        _shop.updateListing(
            _soldPerk1,
            seller2, 
            IListing.PaymentMethod(address(_perkManager), _exclusePerk, IListing.PaymentType.PERK_REVOKE, admin, address(_paymentManager)),
            IListing.Ware(address(_L7LToken), 0, IListing.WareType.ERC20_TRANSFER, 100000, 1, 1000, address(_deliveryManager))
        );
        (address _seller, IListing.PaymentMethod memory _paymentMethod, IListing.Ware memory _ware) = _shop.listings(_soldPerk1);
        assertTrue(_seller == seller2);
        assertTrue(_paymentMethod.tokenAddress == address(_perkManager));
        assertTrue(_paymentMethod.tokenId == _exclusePerk);
        assertTrue(_paymentMethod.paymentType == IListing.PaymentType.PERK_REVOKE);
        assertTrue(_paymentMethod.beneficiary == admin);
        assertTrue(_paymentMethod.paymentManager == address(_paymentManager));
        assertTrue(_ware.wareAddress == address(_L7LToken));
        assertTrue(_ware.wareId == 0);
        assertTrue(_ware.wareType == IListing.WareType.ERC20_TRANSFER);
        assertTrue(_ware.inventoryAmount == 100000);
        assertTrue(_ware.pricePerUnit == 1);
        assertTrue(_ware.pricePerUnitDivisor == 1000);
        assertTrue(_ware.deliveryManager == address(_deliveryManager));
        vm.stopPrank();
    }

     function testSellerCantBeEmpty() public {
        _defaultListing();

        vm.startPrank(seller1);
        PaymentManager _paymentManager = new PaymentManager(address(_shop));
        _shop.updateListing(
            _soldPerk1,
            address(0), 
            IListing.PaymentMethod(address(_perkManager), _exclusePerk, IListing.PaymentType.PERK_REVOKE, admin, address(_paymentManager)),
            IListing.Ware(address(_L7LToken), 0, IListing.WareType.ERC20_TRANSFER, 100000, 1, 1000, address(_deliveryManager))
        );
        (address _seller,,) = _shop.listings(_soldPerk1);
        assertTrue(_seller == seller1);
        vm.stopPrank();
    }

    function testOtherSellersCantUpdateListings() public {
        _defaultListing();

        vm.startPrank(admin);
        _shop.grantRole(_shop.SELLER_ROLE(), seller2);
        vm.stopPrank();

        vm.startPrank(seller2);
        PaymentManager _paymentManager = new PaymentManager(address(_shop));
        vm.expectRevert(Shop.Unauthorized.selector);
        _shop.updateListing(
            _soldPerk1,
            seller2, 
            IListing.PaymentMethod(address(_perkManager), _exclusePerk, IListing.PaymentType.PERK_REVOKE, admin, address(_paymentManager)),
            IListing.Ware(address(_L7LToken), 0, IListing.WareType.ERC20_TRANSFER, 100000, 1, 1000, address(_deliveryManager))
        );
        vm.stopPrank();
    }

    function testAdminCantUpdateListings() public {
        _defaultListing();

        vm.startPrank(admin);
        PaymentManager _paymentManager = new PaymentManager(address(_shop));
        vm.expectRevert(Shop.Unauthorized.selector);
        _shop.updateListing(
            _soldPerk1,
            seller2, 
            IListing.PaymentMethod(address(_perkManager), _exclusePerk, IListing.PaymentType.PERK_REVOKE, admin, address(_paymentManager)),
            IListing.Ware(address(_L7LToken), 0, IListing.WareType.ERC20_TRANSFER, 100000, 1, 1000, address(_deliveryManager))
        );
        vm.stopPrank();
    }

    function testAdminCanDelist() public {
        _defaultListing();

        vm.startPrank(admin);
        _MultiMinter.mint(buyer2, 100 ether);
        _shop.delist(_soldPerk1);
        vm.stopPrank();

        vm.startPrank(buyer2);
        _L7LToken.approve(address(_paymentManager), 100 ether);
        vm.expectRevert(Shop.InvalidListing.selector);
        _shop.exchange(_soldPerk1, 100 ether, 1, "\x00");
        vm.stopPrank();
    }

    function testScam3rdPartyListing() public {
        _defaultListing();

        vm.startPrank(admin);
        _shop.grantRole(_shop.SELLER_ROLE(), seller2);
        vm.stopPrank();

        vm.startPrank(seller2);
        _shop.list(
            keccak256("SCAM"),
            IListing.PaymentMethod(address(_L7LToken), 0, IListing.PaymentType.ERC20_TRANSFER, seller1, address(0)),
            IListing.Ware(address(_perkManager), _exclusePerk, IListing.WareType.PERK_EXCLUSIVE, 5, 1, 1, address(_deliveryManager))
        );
        vm.stopPrank();

        vm.startPrank(buyer1);
        _L7LToken.approve(address(_paymentManager), 1);
        vm.expectRevert(IDeliveryManager.Unauthorized.selector);
        _shop.exchange(keccak256("SCAM"), 1, 1, "\x00");
        vm.stopPrank();
    }

    function testDeliveryManagerAdminAccessControls() public {
        vm.startPrank(admin);
        _deliveryManager = new PerkDeliveryManager(admin, address(_shop));
        assertTrue(_deliveryManager.authorizedListings(keccak256("LISTING")) == false);
        _deliveryManager.adminToggleListingAuthorization(keccak256("LISTING"), true);
        assertTrue(_deliveryManager.authorizedListings(keccak256("LISTING")));
        _deliveryManager.adminToggleListingAuthorization(keccak256("LISTING"), false);
        assertTrue(_deliveryManager.authorizedListings(keccak256("LISTING")) == false);
        assertTrue(_deliveryManager.authorizedWareAddresses(address(_perkManager)) == false);
        _deliveryManager.adminToggleWareAddressAuthorization(address(_perkManager), true);
        assertTrue(_deliveryManager.authorizedWareAddresses(address(_perkManager)));
        _deliveryManager.adminToggleWareAddressAuthorization(address(_perkManager), false);
        assertTrue(_deliveryManager.authorizedWareAddresses(address(_perkManager)) == false);
        _deliveryManager.adminAuthorizeWareListing(keccak256("LISTING"), address(_perkManager));
        assertTrue(_deliveryManager.authorizedListings(keccak256("LISTING")));
        assertTrue(_deliveryManager.authorizedWareAddresses(address(_perkManager)));
        vm.stopPrank();
    }

    function testDeliveryManagerAccessControlsAuth() public {
        vm.startPrank(admin);
        _deliveryManager = new PerkDeliveryManager(admin, address(_shop));
        vm.stopPrank();

        vm.startPrank(seller1);
        vm.expectRevert(abi.encodeWithSelector(Ownable.OwnableUnauthorizedAccount.selector, seller1));
        _deliveryManager.adminToggleListingAuthorization(keccak256("LISTING"), true);
        vm.expectRevert(abi.encodeWithSelector(Ownable.OwnableUnauthorizedAccount.selector, seller1));
        _deliveryManager.adminToggleWareAddressAuthorization(address(_perkManager), true);
        vm.expectRevert(abi.encodeWithSelector(Ownable.OwnableUnauthorizedAccount.selector, seller1));
        _deliveryManager.adminAuthorizeWareListing(keccak256("LISTING"), address(_perkManager));
        vm.stopPrank();
    }


    /////////////
    // HELPERS //
    /////////////

    function _defaultListing() private {
        vm.startPrank(seller1);
        _shop.list(
            _soldPerk1,
            IListing.PaymentMethod(address(_L7LToken), 0, IListing.PaymentType.ERC20_TRANSFER, seller1, address(0)),
            IListing.Ware(address(_perkManager), _exclusePerk, IListing.WareType.PERK_EXCLUSIVE, 5, 100 ether, 1, address(_deliveryManager))
        );
        vm.stopPrank();

        vm.prank(admin);
        _deliveryManager.adminToggleListingAuthorization(_soldPerk1, true);
    }
}
