// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import {Test} from "forge-std/Test.sol";
import {IMinter} from "src/interfaces/IMinter.sol";
import {L7LToken} from "src/token/L7LToken.sol";
import {MultiMinter} from "src/MultiMinter.sol";
import {EndpointV2Mock} from "@layerzerolabs/test-devtools-evm-foundry/contracts/mocks/EndpointV2Mock.sol";

contract TestL7LWithDeployments is Test {
    L7LToken internal _L7LToken;
    MultiMinter internal _MultiMinter;
    EndpointV2Mock internal _endpoint;

    function deployAll(address admin) internal {
        _endpoint = new EndpointV2Mock(30101, admin);
        _L7LToken = new L7LToken(address(_endpoint), admin);
        _MultiMinter = new MultiMinter(admin, IMinter(address(_L7LToken)));
        _L7LToken.transferMinter(address(_MultiMinter));
        _MultiMinter.adminAcceptMinter();
    }
}