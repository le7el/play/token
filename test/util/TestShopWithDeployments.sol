// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import {TestL7LWithDeployments} from "./TestL7LWithDeployments.sol";
import {Shop} from "src/swap/Shop.sol";
import {PerkManager} from "src/swap/shop/PerkManager.sol";
import {PaymentManager} from "src/swap/shop/PaymentManager.sol";
import {PerkDeliveryManager} from "src/swap/shop/PerkDeliveryManager.sol";

contract TestShopWithDeployments is TestL7LWithDeployments {
    Shop internal _shop;
    PerkManager internal _perkManager;
    PaymentManager internal _paymentManager;
    PerkDeliveryManager internal _deliveryManager;
    uint256 internal _exclusePerk;
    bytes32 internal _soldPerk1 = keccak256("PERK");
    
    function deployAll(address admin, address seller) internal {
        super.deployAll(admin);

        _shop = new Shop(admin, admin);
        _perkManager = new PerkManager();
        _paymentManager = new PaymentManager(address(_shop));
        _shop.adminChangeDefaultPaymentManager(_paymentManager);
        _deliveryManager = new PerkDeliveryManager(admin, address(_shop));
        _deliveryManager.adminToggleWareAddressAuthorization(address(_perkManager), true);
        _exclusePerk = _perkManager.registerPerk(admin, 10, true, "\x00");
        _perkManager.togglePerkManager(_exclusePerk, address(_deliveryManager), true);
        _shop.grantRole(_shop.SELLER_ROLE(), seller);
    }
}