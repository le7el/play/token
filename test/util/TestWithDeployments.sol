// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.20;

import "forge-std/Test.sol";
import {Swap} from "src/swap/Swap.sol";
import {EESeedToken} from "src/token/EESeedToken.sol";
import {PaymentTokenMock} from "src/mocks/PaymentTokenMock.sol";

contract TestSwapWithDeployments is Test {
    bytes internal constant VALIDATOR_KEY = abi.encode(0xD1356C7617737cD8078dE7F24781BD3Ac7E79018);

    uint internal EXCHANGE_RATE = 350;
    uint internal EXCHANGE_BASE = 10000000000000000; // USDC / USDT have 6 decimals + 3 decimals for (0.0378 rate), so 18-6+4=16 zeros
    uint internal BONUS_EXCHANGE_RATE = 378;

    Swap internal swap;
    EESeedToken internal vendorToken;
    PaymentTokenMock internal paymentToken;

    function deployAll(address admin) internal {
        paymentToken = new PaymentTokenMock(admin);
        vendorToken = new EESeedToken(admin);
        swap = new Swap(
            admin,
            admin,
            address(vendorToken),
            address(paymentToken),
            EXCHANGE_RATE,
            EXCHANGE_BASE,
            BONUS_EXCHANGE_RATE,
            VALIDATOR_KEY
        );
    }
}