const path = require('path');

module.exports = (env, argv) => { 
  return {
    mode: argv.mode === 'production' ? 'production' : 'development',
    devtool: 'inline-source-map',
    entry: {
      lib: './js/lib.ts',
    },
    output: {
      filename: '[name].js',
      library: {
        name: 'l7l',
        type: 'umd',
      }
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: [/node_modules/, path.resolve(__dirname, 'l0_cli')]
        },
      ],
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
    }
  }
}